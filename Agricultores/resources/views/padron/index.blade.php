@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->


<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">

                
                
                 
<!-- Filtros -->
<nav class="navbar navbar-light float-right" style="background-color:rgba(0, 120, 155,  0.6);">
  <h2 class="text-white"><strong>Padrón</strong></h2>

  <form class="form-inline">

      <select name="tipo" class="form-control mr-sm-2"  required>
        <option selected>Buscar por...</option>
        <option>Nombre</option>
        <option>Tarjeta</option>
        <option>Ejido</option>
      </select>

    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Buscar" required>
    <button class="btn btn-link" type="submit" >
      <h2 style="margin-top: 10px; margin-left: -10px;"><i class="fas fa-search text-white"></i></h2>
    </button>
    
    <a href="{{ route('padron.index') }}" style="margin-top: 10px; margin-left: 5px"><h2><i class="fas fa-sync-alt text-white"></i></h2></a>

  </form>

</nav>
<!-- Fin Filtros -->

<div class="card-body">
 <table class="table table-hover table-responsive-lg ">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Nombre</th>
      <th scope="col">Tarjeta</th>
      <th scope="col">Ejido</th>
      <th colspan="3">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
     @foreach($padron as $item)
                        <tr>
                          
                            <td>{{$item->nombre}}</td>
                              <td>{{$item->tarjeta}}</td>
                                <td>{{$item->ejido}}</td>

                                      <td width="10px">
                                 
                                          <a href="{{route('padron.show',$item->id)}}" class="btn btn-sm btn-default">
                                            <i class="far fa-eye"></i>
                                          </a>
                       
                                      </td>
                                      <td width="10px">
                       
                                          <a href="{{ route('padron.edit',$item->id) }}" class="btn btn-sm btn-default" >
                                            <i class="fas fa-edit"></i>
                                          </a>

                                          </td>

                                          <td width="10px">
                            
                                              <form method="post" action="{{route('padron.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este registro del padrón?');">
                                                      <i class="far fa-trash-alt"></i>    
                                                  </button>
                                              </form>  
                        
                                           </td>
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>

 
                 

                    {{--Paginación--}}

                     <div class="row justify-content-center responsive">                       
                        {{$padron->onEachSide(2)->links()}}                  
                     </div>

                </div>
            </div>
        </div>
    </div>
</div>

  {{-- Boton arriba flotante --}}
  <a href="#" class="btn btn-info back-to-top">
              <i class="fas fa-chevron-up"></i>
  </a>


@endsection