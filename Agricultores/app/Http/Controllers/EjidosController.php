<?php

namespace App\Http\Controllers;

use App\Http\Requests\EjidoStoreRequest;
use App\Http\Requests\EjidoUpdateRequest;
use App\Ejidos;
use DB;
use Illuminate\Http\Request;

class EjidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
   
       $ejidos = Ejidos::paginate(15);
        return view('ejidos.index',compact('ejidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EjidoStoreRequest $request)
    {
        //
      
        $datosEjido=request()->except('_token');
        Ejidos::insert($datosEjido);
 
        return redirect('/ejidos')->with('Mensaje','¡Ejido guardado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ejidos  $ejidos
     * @return \Illuminate\Http\Response
     */
    public function show(Ejidos $ejidos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ejidos  $ejidos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ejidos = DB::table('ejidos')
                    ->where('id', '=', $id)
                    ->get();
        return view('ejidos.editar',compact('ejidos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ejidos  $ejidos
     * @return \Illuminate\Http\Response
     */
    public function update(EjidoUpdateRequest $request, $id)
    {
        //
        $datosEjido=request()->except(['_token','_method']);
         Ejidos::where('id','=',$id)->update($datosEjido);

        return redirect('/ejidos')->with('Mensaje','¡Ejido modificado exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ejidos  $ejidos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Ejidos::destroy($id);
        return redirect('/ejidos')->with('Mensaje','¡Ejido eliminado exitosamente!');
    }

     public function agregar()
    {
        //
        return view('ejidos.nuevo');
    }
}
