@extends('admin.layout')

@section('content')

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow p-1">

			@foreach($Adeudo as $item)
                <div class="card-header h5 font-weight-bold text-white" style="background-color: #8FD502">
                    No.Tarjeta: {{$item->tarjeta}}

                </div>

                <div class="card-body">
                    <table class="table">
                        <th scope="row">Fecha:</th>
                        <td>{{$item->fecha}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Nombre:</th>
                        <td>{{$item->usuario}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cuenta:</th>
                        <td>{{$item->cuenta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Municipio:</th>
                        <td>{{$item->municipio}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Ejido:</th>
                        <td>{{$item->ejido}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Seccion:</th>
                        <td>{{$item->seccion}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SUP_FIS:</th>
                        <td>{{$item->superficie}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Concepto:</th>
                        <td>{{$item->concepto}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Riegos:</th>
                        <td>{{$item->riegos}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cantidad:</th>
                        <td>{{$item->cantidad}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Importe:</th>
                        <td class="bg-warning">$ {{$item->importe}}</td>
                        </tr>            
                        
                    </table>

                      <div class="text-right">
                          <a href="{{ route('adeudos.index')}}" class="btn btn-sm btn-link">
                            <h1> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
             
            </div>
            @endforeach
        </div> 

    </div>



    
</div>



@endsection