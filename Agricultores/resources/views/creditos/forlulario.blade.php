@extends('admin.layout')

@section('content')

 <!-- Muestro mensaje de exito -->
        @if(Session::has('Mensaje'))
           <div class="alert alert-success" role="alert">
               {{Session::get('Mensaje')}}
           </div>
        @endif
<!-- Fin mensaje-->

<br/>
<form method="POST" action="{{route('creditos.store')}}">
  @csrf

<div class="container bg-white shadow p-2" >

   <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Solicitud de crédito') }}</strong></h4></div>
    
   <br/>

     <div class="row justify-content-center ">
          <div class="col-md-12">
              <div class="form-group">
                    <label for="" class="text-secondary">Nombre Completo:</label>
                    <input type="text" value="{{ old('nombre') }}" class="form-control {{$errors->has('nombre')?'is-invalid':''}}" name="nombre" id="nombre">
                      @error('nombre')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                              </span>
                       @enderror

              </div>
                    
          </div>

          

          <div class="col-md-12">
              <div class="form-group">
                    <label for="" class="text-secondary">Tarjeta:</label>
                    <input type="text" value="{{ old('tarjeta') }}" class="form-control {{$errors->has('tarjeta')?'is-invalid':''}}" name="tarjeta" id="tarjeta">
                      @error('tarjeta')
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                </span>
                         @enderror
                </div>
                
          </div>
            
          

          


          <div class="col-md-12">
              <div class="form-group">
                    <label for="" class="text-secondary">Telefono:</label>
                    <input type="text" value="{{ old('telefono') }}" class="form-control {{$errors->has('telefono')?'is-invalid':''}}" name="telefono" id="telefono">
                       @error('telefono')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                              </span>
                       @enderror
              </div>
               
                
          </div>

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary">Calle:</label>
                    <input type="text" value="{{ old('calle') }}" class="form-control {{$errors->has('calle')?'is-invalid':''}}" name="calle" id="calle">
                      @error('calle')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                              </span>
                       @enderror


              </div>
                
          </div>

          

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary"> Número exterior:</label>
                    <input type="text" value="NA" class="form-control {{$errors->has('noexterior')?'is-invalid':''}}" name="noexterior" id="noexterior">
                      @error('noexterior')
                                  <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                  </span>
                           @enderror
                </div>
                
          </div>

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary">Número interior:</label>
                    <input type="text" value="NA" class="form-control {{$errors->has('nointerior')?'is-invalid':''}}" name="nointerior" id="nointerior">
                      @error('nointerior')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                              </span>
                       @enderror
              </div>
                    
          </div>

          

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary">Código Postal:</label>
                    <input type="text" value="{{ old('cpostal') }}" class="form-control {{$errors->has('cpostal')?'is-invalid':''}}" name="cpostal" id="postal">
                      @error('cpostal')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                </div>
                
          </div>

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary">Delegación o Municipio:</label>
                    <input type="text" value="{{ old('municipio') }}" class="form-control {{$errors->has('municipio')?'is-invalid':''}}" name="municipio" id="municipio">
                      @error('municipio')
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                </span>
                         @enderror
              </div>
                
          </div>

          

          <div class="col-md-6">
              <div class="form-group">
                    <label for="" class="text-secondary">Estado:</label>
                    <input type="text" value="{{ old('estado') }}" class="form-control {{$errors->has('estado')?'is-invalid':''}}" name="estado" id="estado">
                      @error('estado')
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                </span>
                         @enderror
                </div>
                
          </div>

          

          



          <!-- termina formulario -->


<br/>
               <div class="row">
                            <div class="col text-center">
                               <a href="{{ route('home') }}" class="btn btn-link">
                                   
                                   <h1> <i class="fas fa-times"></i></h1> 
                                    
                                 </a>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                     <h1> <i class="fas fa-check"></i></h1> 
                                </button>
                            </div>

              </div>
                 <br/>
</div>

  
</form>

@endsection