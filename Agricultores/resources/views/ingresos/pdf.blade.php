<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style>
		body{
			font-family: sans-serif;
		}
		.logo{
			width: 25%;
			text-align: left;
			margin-bottom: -30px;
		}
		
		.tabla{
			width: 100%;
			border: 2px solid #999999;
			border-collapse: collapse;
			margin: 2px;
  			padding: 2px;
			
		}

		.tbody{
			text-align: center;
			border: 2px solid #999999;
			border-collapse: collapse;
   			padding: 0.3em;

		}
		th {
		   background: #eee;
		}
		td{
			border: 1px solid #999999;
		}
		h3{
			text-align: center;
			margin-top: -50px;
			font-weight: bold;

		}
		
	</style>

	 <img class="logo" src={{asset('Images/logo.jpg')}} alt="Logo"/>
	

</head>
<body>
 <h3>Ingresos</h3>
	<table class="tabla">
		<thead>
			<tr>
				<th>Factura</th>
				<th>Fecha</th>
				<th>Nombre</th>
				<th>Cuenta</th>
				<th>Ejido</th>	
				<th>Riegos</th>	
				<th>Total</th>	
			</tr>
		</thead>
		<tbody class="tbody">
			@foreach($datosIngresos as $item)
			<tr>
				<td>{{ $item->factura }}</td>
				<td>{{ $item->fecha }}</td>
				<td>{{ $item->nombre }}</td>
				<td>{{ $item->cuenta }}</td>
				<td>{{ $item->ejido }}</td>
				<td>{{ $item->riegos }}</td>
				<td>{{ $item->total }}</td>
			</tr>
			@endforeach


		</tbody>
	</table>
</body>
</html>