@extends('admin.layout')

@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gráficas</title>

    <style>
        .content{

            text-align:center;
        }

    </style>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Cultivo', 'Porcentaje '],
          ['MAIZ PV',     91],
          ['ALFALFA PER',      247],
          ['AGRIC',  21],
          ['SORGO PV',  51],
          ['TRIGO OL',  83],
          ['PRODEP',  20],
          ['AVENA OL',  40],
          
        ]);

        var options = {
          //title: 'My Daily Activities',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['MUNICIPIO', 'Porcentaje '],
          ['ALVARO OBREGON',     28],
          ['TARIMBARO',      6],
          
          
        ]);

        var options = {
          //title: 'My Daily Activities',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart2'));
        chart.draw(data, options);
      }
    </script>

</head>
<body>
  <br/>
    <div class="container">
      <div class="card shadow">
        <div>
            <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                       Cultivos
            </div>
       
            <br/>
          <div id="donutchart" style="width: 1000px; height: 500px;"></div>
          </div>
    </div>
  </div>

    <div class="container">
        <div class="card shadow">
        <div>
            <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                       Ejidos
            </div>
        <br>
        <div id="donutchart2 " style="width: 1000px; height: 500px;"></div>
    </div>
  </div>
</div>

    
</body>
</html>

@endsection