<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class H1 extends Model
{
    //
     protected $table = 'h1';
     protected $fillable = [
        'FACTURA',
        'FECHA',
        'TARJETA',
        'USUARIO',
        'CICLOACTUAL',
        'CUENTA',
        'SEC',
        'EJI',
        'NOMBRE_DE_EJIDO',
        'MPIO',
        'CULTIVO',
        'SUP_FISICA_TOTAL',
        'CONCEPTO',
        'TIPO',
        'CICLO_AÑO',
        'UNIDAD',
        'CUOTA_POR_UNIDAD',
        'RIEGOS',
        'CANTIDAD',
        'IMPORTE',
    ];

    public function scopeBuscarpor($query, $tipo, $buscar){
        if(($tipo) && ($buscar)){
            return $query->where($tipo,'like',"%$buscar%");
        }
    }
}
