<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->id();

            $table->string('folio');
            $table->date('fecha');
            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('tarjeta');
            $table->float('cuenta');
            $table->float('importe');
            $table->float('cantidad');
            $table->string('ciclo_ao');
            $table->bigInteger('id_concepto')->unsigned();
            $table->foreign('id_concepto')->references('id')->on('concepto');
            $table->string('unidad');
            $table->float('cuota');
            $table->integer('riegos');
            $table->float('total');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
