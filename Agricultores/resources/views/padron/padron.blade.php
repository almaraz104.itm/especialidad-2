@extends('admin.layout')

@section('content')

<br/>

<!-- Filtros -->
<nav class="navbar navbar-light " style="background-color:rgba(0, 120, 155,  0.6);">
  <h2 class="text-white"><strong>Padrón Completo</strong></h2>

  <form class="form-inline">

      <select name="tipo" class="form-control mr-sm-2"  required>
        <option selected>Buscar por...</option>
         <option>Nombre</option>
        <option>Tarjeta</option>
        <option>Ejido</option>
      </select>

    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Buscar" required>
    <button class="btn btn-link" type="submit">
       <h2 style="margin-top: 10px; margin-left: -10px"><i class="fas fa-search text-white"></i></h2>
    </button>

    <a href="{{ route('padron.todo') }}" style="margin-top: 10px; margin-left: 5px"><h2><i class="fas fa-sync-alt text-white"></i></h2></a>

  </form>


</nav>
<!-- Fin Filtros -->


<table class="table table-bordered table-responsive">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Nombre</th>
      <th scope="col">Tarjeta</th>
      <th scope="col">Cuenta</th>
      <th scope="col">Municipio</th>
      <th scope="col">Ejido</th>
      <th scope="col">Seccion</th>
      <th scope="col">Sup_fis</th>
      <th scope="col">Ejido_no</th>
      <th scope="col">Sistema</th>
      <th scope="col">Canelero</th>
      <th scope="col">S_cta</th>
      <th scope="col">Doc</th>
      <th scope="col">No</th>
      <th scope="col">Fecha</th>
      <th scope="col">Tarjeta_ant</th>
      <th scope="col">Usipad</th>
      <th scope="col">Cta_sipad</th>
      <th scope="col">Sup_fisica</th>
      <th scope="col">Sup_riego</th>
      <th scope="col">Sec_orig</th>
    </tr>
  </thead>
  <tbody class="table-warning">
  	 @foreach($padron as $item)
                        <tr>
                       		
                            <td>{{$item->nombre}}</td>
                              <td>{{$item->tarjeta}}</td>
                                <td>{{$item->cuenta}}</td>
                                  <td>{{$item->municipio}}</td>
                                  	<td>{{$item->ejido}}</td>
                                       <td>{{$item->seccion}}</td>
                                  <td>{{$item->sup_fis}}</td>
                                <td>{{$item->ejido_no}}</td>
                              <td>{{$item->sistema}}</td>
                            <td>{{$item->canalero}}</td>
                              <td>{{$item->s_cta}}</td>
                                <td>{{$item->doc}}</td>
                                  <td>{{$item->no}}</td>
                                    <td>{{$item->fecha}}</td>
                                      <td>{{$item->tarjeta_ant}}</td>
                                        <td>{{$item->usipad}}</td>
                                        <td>{{$item->cta_sipad}}</td>
                                      <td>{{$item->sup_fisica}}</td>
                                    <td>{{$item->sup_riego}}</td>
                                  <td>{{$item->sec_orig}}</td>

                        </tr>
                        <tr>
                        	
                        </tr>
                 

    @endforeach
  </tbody>
</table>

{{--Paginación--}}
    <div class="row justify-content-center responsive">                       
       {{$padron->onEachSide(2)->links()}}                  
    </div>
    
{{-- Boton arriba flotante --}}
<a href="#" class="btn btn-info back-to-top">
            <i class="fas fa-chevron-up"></i>
</a>

@endsection