<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Asociación de Agricultores-Del Valle') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/util.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>
<body>
  <!--
   <div style="background-color: #94F008; color:  #34495e " >
      <strong> <h4 style="text-align: center;">{{ __('Asosiación de Agricultores del Valle Álvaro Obregón-Tarimbaro A.C.') }}</strong></h4>
</div> -->
   <!--
                         <ul>
                                <li class="nav-item" >
                                    <a class="nav-link" href="{{ route('register') }}">
      <strong> <h5 style="text-align: center; color: #FFFFFF">{{ __('Asosiación de Agricultores del Valle Álvaro Obregón-Tarimbaro A.C.') }}</strong></h5></a>
                                </li>
                        </ul>
              -->        
                   

        <main class="py-0">
            @yield('content')
        </main>
    </div>
</body>
</html>
