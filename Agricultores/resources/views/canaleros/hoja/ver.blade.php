<!DOCTYPE html>
<html>
<head>
  <title>Libro-Canaleros</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <a href="{{ route('hoja.index') }}">
   <img src={{asset('Images/logo.jpg')}} alt="Logo" class=""
           style="opacity: .5; width: 150px">
  </a>
<table class="table table-bordered">

  <thead>
    <tr class="">
     
      <th scope="col">NOMBRE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TARJETA&nbsp; CPL&nbsp; TSL &nbsp;RA&nbsp; PCO&nbsp; SR&nbsp; SUP_ FIS</th>
       
                      <th scope="col">P DREN</th>
                        <th scope="col">S DREN</th>
                        <th scope="col">CULTIVO</th>
                      <th scope="col">HF</th>
                    <th scope="col">P1</th>
                  <th scope="col">R1</th>
                <th scope="col">T1</th>
              <th scope="col">P2</th>
            <th scope="col">R2</th>
      
    </tr>
  </thead>
  <tbody>
    @foreach($libro as $item)
  
                        <tr>
                          
                            <td>{{ $item->nombre }} &nbsp;
                                {{ $item->tarjeta }}&nbsp;
                                {{ $item->cpl }}&nbsp;
                                {{ $item->tsl }}&nbsp;&nbsp;&nbsp;&nbsp;
                                {{ $item->ra }}&nbsp;&nbsp;&nbsp;
                                {{ $item->pco }}&nbsp;&nbsp;&nbsp;
                                {{ $item->sr }}&nbsp;&nbsp;&nbsp;&nbsp;
                                {{ $item->sup_fis}}
                              </td>
      
                                          <td>{{ $item->p_dren}}</td>
                                        <td>{{ $item->s_dren }}</td>
                                      <td>{{ $item->cultivo}}</td>
                                    <td>{{ $item->hf }}</td>
                                  <td>{{ $item->p1 }}</td>
                                <td>{{ $item->r1 }}</td>
                              <td>{{ $item->t1 }}</td>
                            <td>{{ $item->p2}}</td>
                          <td>{{ $item->r2 }}</td>
                                  
                        </tr>
          

    @endforeach         
  </tbody>
</table>


<script type="text/javascript">
function imprimir()
{
  var Obj = document.getElementById("ocultar");
  Obj.style.visibility = 'hidden';
  window.print();
}
</script>

<div class="col text-center">
<input type="button" id="ocultar" onClick="imprimir()" value="Imprimir" class="btn btn-warning"> 
</div>

</body>
</html>