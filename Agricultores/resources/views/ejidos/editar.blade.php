@extends('admin.layout')

@section('content')



<br/>

@foreach($ejidos as $item)
 <div class="row justify-content-center">
        <div class="col-md-10 shadow p-2">
            <div class="card shadow">
               <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>Editar Ejido: {{$item->nombre  }}</strong></h4></div>

                
                    <form method="POST" action="{{route('ejidos.update',$item->id)}}">
                        @csrf
                        {{method_field('PATCH')}}
                        <br/>
                        <div class="form-group row">
                            <label for="clave" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('nombre') is-invalid @enderror " name="nombre" id="nombre" value="{{$item->nombre}}" required autocomplete="nombre" style="text-transform:capitalize;"> @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                                                      
                           </div>
                       </div>

                          <div class="form-group row">
                            <label for="municipio" class="col-md-4 col-form-label text-md-right">{{ __('Municipio') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="municipio" value="{{$item->municipio}}">
                                    <option selected>--- Selecciona una opción ---</option>
                                    <option >Alvaro Obregon</option>
                                    <option >Tarimbaro</option>
                                </select>                        
                           </div>
                       </div>
 
                         <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('ejidos.index') }}" class="btn btn-link">
                                   
                                   <h1> <i class="fas fa-times"></i></h1>  
                                    
                                 </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                   <h1> <i class="far fa-save"></i></h1>
                                </button>
                            </div>
                        </div>
    <br/>
                         

                    </form>

                </div>
            </div>
        </div>

@endforeach
@endsection







