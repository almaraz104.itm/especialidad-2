@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))

    <div class="alert alert-success " role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow "> 
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                   Canaleros
                   <div class="text-right">
                    
                        <a  href="{{ route('canaleros.nuevo') }}">
                            <button class="btn pmd-btn-fab pmd-ripple-effect btn-info" type="button"><i class="fas fa-user-plus"></i></button></button>
                        </a>
                    
                   </div>
                </div>

                <div class="card-body">

<table class="table table-hover table-responsive-lg">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Nombre</th>
      <th scope="col">Zona</th>
      <th scope="col">Direccion</th>
      <th scope="col">teléfono</th>
      <th colspan="3">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
     @foreach($canaleros as $item)
                        <tr>
                          
                            <td>{{$item->nombre}}</td>
                              <td>{{$item->seccion}}</td>
                                <td>{{$item->direccion}}</td>
                                  <td>{{$item->telefono}}</td>
                                      <td width="10px">
                       
                                          <a href="{{ route('canaleros.edit',$item->id) }}" class="btn btn-sm btn-default" >
                                            <i class="fas fa-edit"></i>
                                          </a>

                                          </td>

                                          <td width="10px">
                            
                                              <form method="post" action="{{route('canaleros.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este registro?');">
                                                      <i class="far fa-trash-alt"></i>    
                                                  </button>
                                              </form>  
                        
                                           </td>
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>
   

                </div>
            </div>
        </div>
    </div>
</div>






@endsection