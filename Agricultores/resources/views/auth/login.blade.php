@extends('layouts.app')

@section('content')



<div class="container-login100">
    
    <div class="">

        <div class="card shadow mb-5 bg-light" style="margin-top:-15%" >

             <div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">

                 <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <span class="login100-form-title p-b-50">
                        Inicia Sesión
                    </span>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror input100" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="Usuario">

                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <img class="" src={{asset('images/Icon/usuariologin.png')}}  />
                        </span>

                            @error('username')
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                             @enderror                          
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror input100" name="password" value="{{ old('password') }}" required autocomplete="password" placeholder="Contraseña">

                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <img class="" src={{asset('images/Icon/pass.png')}}  />
                            </span>

                                @error('password')
                                     <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror                          
                     </div>


                    <div class="container-login100-form-btn p-t-25">
                        <button class="login100-form-btn" type="submit">
                            Entrar
                        </button>
                    </div>

                
                 </form>
                
             </div>
         </div>
     </div>
</div>

@endsection
