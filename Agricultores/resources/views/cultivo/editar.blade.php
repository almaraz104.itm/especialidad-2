@extends('admin.layout')

@section('content')



<br/>

@foreach($cultivos as $item)
 <div class="row justify-content-center">
        <div class="col-md-10 shadow p-2">
            <div class="card">
               <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>Editar Cultivo: {{$item->cultivo  }}</strong></h4></div>

                <div class="card-body">
                    <form method="POST" action="{{route('cultivo.update',$item->id)}}">
                        @csrf
                        {{method_field('PATCH')}}

                        <div class="form-group row">
                            <label for="clave" class="col-md-4 col-form-label text-md-right">{{ __('Clave') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('clave')?'is-invalid':''}} " name="clave" id="clave" value="{{$item->clave}}" disabled="">  
                                <div class="invalid-feedback"> 
                                    Clave ya registrada
                                </div>                         
                           </div>
                       </div>

                          <div class="form-group row">
                            <label for="cultivo" class="col-md-4 col-form-label text-md-right">{{ __('Cultivo') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('cultivo')?'is-invalid':''}} " name="cultivo" id="cultivo" value="{{ $item->cultivo }}">  
                                <div class="invalid-feedback"> 
                                    El campo Cultivo es obligatorio
                                </div>                         
                           </div>
                       </div>

                        <div class="form-group row">
                            <label for="fsiembra" class="col-md-4 col-form-label text-md-right">{{ __('Siembra') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('fsiembra')?'is-invalid':''}}" name="fsiembra" id="fsiembra" value="{{ $item->fsiembra }}">
                                <div class="invalid-feedback"> 
                                    El campo Siembra es obligatorio
                                </div>             
                            </div>
                        </div>
 
                        <div class="form-group row">
                            <label for="fcosecha" class="col-md-4 col-form-label text-md-right">{{ __('Cosecha') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('fcosecha')?'is-invalid':''}}" name="fcosecha" id="fcosecha" value="{{ $item->fcosecha }}"> 
                                <div class="invalid-feedback"> 
                                    El campo Cosecha es obligatorio
                                </div>            
                            </div>
                        </div>

            
                         <div class="form-group row">
                            <label for="fsoca" class="col-md-4 col-form-label text-md-right">{{ __('Soca') }}</label>
                            <div class="col-md-6">
                                <input  type="text" class="form-control {{$errors->has('fsoca')?'is-invalid':''}}" name="fsoca" id="fsoca" value="{{ $item->fsoca }}">
                                <div class="invalid-feedback"> 
                                    El campo Soca es obligatorio
                                </div> 
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="fuente_ab" class="col-md-4 col-form-label text-md-right">{{ __('Fuente') }}</label>
                            <div class="col-md-6">
                                <input  type="text" class="form-control {{$errors->has('fuente_ab')?'is-invalid':''}}" name="fuente_ab" id="fuente_ab" value="{{ $item->fuente_ab }}">
                                <div class="invalid-feedback"> 
                                    El campo Fuente es obligatorio
                                </div> 
                            </div>
                        </div>

                         <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('cultivo.index') }}" class="btn btn-link">
                                   
                                    <h1> <i class="fas fa-times"></i></h1>  
                                    
                                 </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                    <h1> <i class="far fa-save"></i></h1> 
                                </button>
                            </div>
                        </div>
    
                         </div>
                    </form>
                </div>
            </div>
        </div>

@endforeach
@endsection







