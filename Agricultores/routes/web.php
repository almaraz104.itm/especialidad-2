<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


//Rutas Bloqueadas Solo se permite el acceso a usuarios autenticados
Route::group(['middleware' => 'auth'], function () {
//Rutas Cobros
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/cobros/store','HomeController@store')->name('cobros.store');
Route::get('/recibo/pdf', 'HomeController@recibo')->name('recibo.pdf');

//Rutas corte de caja
Route::get('/corte', 'CorteController@index')->name('corte.index');
Route::post('/corte/store','CorteController@store')->name('corte.store');
Route::get('/cortes-de-caja', 'CorteController@verCortes')->name('corte.verCortes');

//Rutas PDF
Route::get('/descargar-pdf-corte', 'CorteController@pdf')->name('descargar.pdf');
Route::get('/descargar-pdf-ingresos', 'IngresosController@pdf')->name('descarga.pdf');


//Rutas Ingresos
Route::get('/ingresos', 'IngresosController@index')->name('Ingresos.index');
Route::get('/ingresos={id}','IngresosController@show')->name('Ingresos.show');
Route::delete('/ingresos={id}','IngresosController@destroy')->name('Ingresos.destroy');

//Rutas Adeudos
Route::get('/adeudos', 'AdeudosController@index')->name('adeudos.index');
Route::get('/adeudos={id}','AdeudosController@show')->name('adeudos.show');
Route::get('/adeudo={id}','AdeudosController@edit')->name('adeudos.edit');
Route::patch('/modificar-adeudo={id}','AdeudosController@update')->name('adeudos.update');


//Rutas Usuarios
Route::get('/usuarios', 'UsuariosController@index')->name('usuarios.index');
Route::get('/registro-usuarios', 'UsuariosController@registry')->name('usuarios.registry');
Route::post('/usuarios/store','UsuariosController@store')->name('usuarios.store');
Route::get('/usuarios={id}','UsuariosController@show')->name('usuarios.show');
Route::get('/usuario={id}','UsuariosController@edit')->name('usuarios.edit');
Route::patch('/modificar-usuario={id}','UsuariosController@update')->name('usuarios.update');
Route::delete('/usuarios={id}','UsuariosController@destroy')->name('usuarios.destroy');

//Rutas Historicos
Route::get('/historico=h1', 'HistoricosController@h1')->name('h1.index');
Route::get('/historico=h2', 'HistoricosController@h2')->name('h2.index');
Route::get('/historico=h3', 'HistoricosController@h3')->name('h3.index');

//Rutas Padrón
Route::get('/padron', 'PadronController@todo')->name('padron.todo');
Route::get('/padron=lista', 'PadronController@index')->name('padron.index');
Route::get('/padron={id}','PadronController@show')->name('padron.show');
Route::get('/nuevo-ejidatario', 'PadronController@agregar')->name('padron.nuevo');
Route::post('/padron/store','PadronController@store')->name('padron.store');
Route::get('/editar-padron={id}','PadronController@edit')->name('padron.edit');
Route::patch('/modificar-padron={id}','PadronController@update')->name('padron.update');
Route::delete('/eliminar-{id}','PadronController@destroy')->name('padron.destroy');

//Rutas Concepto
Route::get('/concepto', 'ConceptoController@index')->name('concepto.index');
Route::get('/nuevo-concepto', 'ConceptoController@agregar')->name('concepto.nuevo');
Route::post('/concepto/store','ConceptoController@store')->name('concepto.store');
Route::get('/concepto={id}','ConceptoController@edit')->name('concepto.edit');
Route::patch('/modificar-concepto={id}','ConceptoController@update')->name('concepto.update');
Route::delete('/concepto={id}','ConceptoController@destroy')->name('concepto.destroy');

//Rutas Cultivo
Route::get('/cultivo', 'CultivoController@index')->name('cultivo.index');
Route::get('/nuevo-cultivo', 'CultivoController@agregar')->name('cultivo.nuevo');
Route::post('/cultivo/store','CultivoController@store')->name('cultivo.store');
Route::get('/cultivo={id}','CultivoController@edit')->name('cultivo.edit');
Route::patch('/modificar-cultivo={id}','CultivoController@update')->name('cultivo.update');
Route::delete('/cultivo={id}','CultivoController@destroy')->name('cultivo.destroy');

//Rutas Ejidos
Route::get('/ejidos', 'EjidosController@index')->name('ejidos.index');
Route::get('/nuevo-ejido', 'EjidosController@agregar')->name('ejidos.nuevo');
Route::post('/ejidos/store','EjidosController@store')->name('ejidos.store');
Route::get('/ejidos={id}','EjidosController@edit')->name('ejidos.edit');
Route::patch('/modificar-ejido={id}','EjidosController@update')->name('ejidos.update');
Route::delete('/ejido={id}','EjidosController@destroy')->name('ejidos.destroy');

//Rutas Canaleros
Route::get('/canaleros', 'CanalerosController@index')->name('canaleros.index');
Route::get('/nuevo-canalero', 'CanalerosController@agregar')->name('canaleros.nuevo');
Route::post('/canaleros/store','CanalerosController@store')->name('canaleros.store');
Route::get('/canaleros={id}','CanalerosController@edit')->name('canaleros.edit');
Route::patch('/modificar-canalero={id}','CanalerosController@update')->name('canaleros.update');
Route::delete('/canalero={id}','CanalerosController@destroy')->name('canaleros.destroy');

//Rutas Libro
Route::get('/libros', 'LibroController@index')->name('libro.index');
Route::get('/libro={id}','LibroController@show')->name('libro.show');
Route::get('/nuevo-libro', 'LibroController@agregar')->name('libro.nuevo');
Route::post('/libro/store','LibroController@store')->name('libro.store');
Route::delete('/eliminar={id}','LibroController@destroy')->name('libro.destroy');
Route::post('/adeudos/libro','LibroController@adeudos')->name('libro.adeudos');

//Rutas Hoja
Route::get('/hoja', 'ImprimirController@index')->name('hoja.index');
Route::post('/hoja/store','ImprimirController@store')->name('hoja.store');
Route::get('/ver/hoja', 'ImprimirController@ver')->name('hoja.ver');
Route::get('/delete','ImprimirController@destroy')->name('eliminar.destroy');


//Rutas Créditos
Route::get('/creditos', 'CreditosController@index')->name('creditos.solicitud');
Route::get('/aprobar', 'CreditosController@aprobarCreditos')->name('creditos.aprobar');
Route::get('/abonos', 'CreditosController@abonos')->name('creditos.abonos');
Route::post('/creditos', 'CreditosController@store')->name('creditos.store');
Route::get('/pagare={id}','CreditosController@edit2')->name('creditos.show');
Route::get('/abono={id}','CreditosController@edit')->name('creditos.edit');
Route::patch('/modificar-abono={id}','CreditosController@update')->name('creditos.update');
Route::patch('/insertar-abono={id}','CreditosController@update2')->name('creditos.update2');
Route::delete('/eliminar={id}','CreditosController@destroy')->name('solicitud.destroy');




//Rutas Gráficas

Route::get('/graficas', 'GraficasController@index')->name('graficas.index');

});