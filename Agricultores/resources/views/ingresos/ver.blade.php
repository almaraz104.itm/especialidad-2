@extends('admin.layout')

@section('content')

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow p-1">

			@foreach($Ingresos as $item)
                <div class="card-header h5 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);">
                    Fecha: {{$item->fecha}}

                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                        <th scope="row">Factura:</th>
                        <td>{{$item->factura}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Tarjeta:</th>
                        <td>{{$item->cuenta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Nombre:</th>
                        <td>{{$item->nombre}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Ciclo:</th>
                        <td>{{$item->ciclo}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cuenta:</th>
                        <td>{{$item->cuenta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Sección:</th>
                        <td>{{$item->seccion}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Ejido:</th>
                        <td>{{$item->ejido}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Municipio:</th>
                        <td>{{$item->municipio}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cultivo:</th>
                        <td>{{$item->cultivo}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Superficie:</th>
                        <td>{{$item->superficie}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Concepto:</th>
                        <td>{{$item->concepto}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Tipo:</th>
                        <td>{{$item->tipo}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Unidad:</th>
                        <td>{{$item->unidad}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cuota:</th>
                        <td>{{$item->cuota2}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Riegos:</th>
                        <td>{{$item->riegos}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cantidad:</th>
                        <td>{{$item->cantidad}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Importe:</th>
                        <td>{{$item->total}}</td>
                        </tr>
                            
                    </table>

                      <div class="text-right">
                          <a href="{{ route('Ingresos.index')}}" class="btn btn-sm btn-link">
                            <h1> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
             
            </div>
            @endforeach
        </div> 

    </div>



    
</div>



@endsection