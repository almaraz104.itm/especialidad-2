@extends('admin.layout')

@section('content')

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow p-1">

			@foreach($padron as $item)
                <div class="card-header h5 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);">
                    N° Tarjeta: {{$item->tarjeta}}

                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                        <th scope="row">Nombre:</th>
                        <td>{{$item->nombre}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cuenta:</th>
                        <td>{{$item->cuenta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Municipio:</th>
                        <td>{{$item->municipio}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Ejido:</th>
                        <td>{{$item->ejido}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Seccion:</th>
                        <td>{{$item->seccion}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SUP_FIS:</th>
                        <td>{{$item->sup_fis}}</td>
                        </tr>
                        <tr>
                        <th scope="row">EJIDO_No:</th>
                        <td>{{$item->ejido_no}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Sistema:</th>
                        <td>{{$item->sistema}}</td>
                        </tr>
                        <tr>
                        <th scope="row">CANALERO:</th>
                        <td>{{$item->canalero}}</td>
                        </tr>
                        <tr>
                        <th scope="row">S_CTA:</th>
                        <td>{{$item->s_cta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">DOC:</th>
                        <td>{{$item->doc}}</td>
                        </tr>
                        <tr>
                        <th scope="row">No:</th>
                        <td>{{$item->no}}</td>
                        </tr>
                        <tr>
                        <th scope="row">FECHA:</th>
                        <td>{{$item->fecha}}</td>
                        </tr>
                        <tr>
                        <th scope="row">TARJETA_ANT:</th>
                        <td>{{$item->tarjeta_ant}}</td>
                        </tr>
                        <tr>
                        <th scope="row">USIPAD:</th>
                        <td>{{$item->usipad}}</td>
                        </tr>
                        <tr>
                        <th scope="row">CTA_SIPAD:</th>
                        <td>{{$item->cta_sipad}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SUP_FISICA:</th>
                        <td>{{$item->sup_fisica}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SUP_RIEGO:</th>
                        <td>{{$item->sup_riego}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SEC_ORIG:</th>
                        <td>{{$item->sec_orig}}</td>
                        </tr>
                        
                        
                    </table>

                      <div class="text-right">
                          <a href="{{ route('padron.index')}}" class="btn btn-sm btn-link">
                            <h1> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
             
            </div>
            @endforeach
        </div> 

    </div>



    
</div>



@endsection