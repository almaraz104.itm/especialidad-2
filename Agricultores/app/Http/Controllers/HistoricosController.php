<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\H1;
use App\H2;
use App\H3;


class HistoricosController extends Controller
{
   

    public function h1(Request $request){

        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $h1 = h1::buscarpor($tipo, $buscar)->paginate(20)->appends($variablesUrl);
        return view('historicos.h1',compact('h1'));

        
    } 

      public function h2(Request $request){

        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $h2 = h2::buscarpor($tipo, $buscar)->paginate(20)->appends($variablesUrl);
        return view('historicos.h2',compact('h2'));

        
    }

     public function h3(Request $request){

        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $h3 = h3::buscarpor($tipo, $buscar)->paginate(20)->appends($variablesUrl);
        return view('historicos.h3',compact('h3'));

        
    }


}
