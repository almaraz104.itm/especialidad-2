<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Asociación de Agricultores-Del Valle</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style-->
  <link rel="stylesheet" href="dist/css/adminlte.min.css"> 
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <!-- Hamburguesa -->
      <h4>
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </h4>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('cultivo.index') }}" class="nav-link">Cultivo</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('concepto.index') }}" class="nav-link">Concepto</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('ejidos.index') }}" class="nav-link">Ejidos</a>
      </li>
    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Home -->
      <li class="nav-item dropdown">
        <a class="nav-link" href="{{ route('home') }}">
          <h4><i class="fas fa-home"></i></h4>
        </a>
       
      </li>

      <!-- Cuadros -->
      <li class="nav-item">
       <h4> <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a></h4>

      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link" style="margin-right: 20px" >
      <img src={{asset('Images/logo.jpg')}} alt="Logo" class="brand-image img-circle elevation-3"
           style="opacity: .5">
      <span class="brand-text font-weight-light"> 
        <img class="img-circle" src={{asset('Images/logo.jpg')}} alt="Logo" height=100% width=80% />
      </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          
        </div>
        <div class="info">
        <a href="{{ route('usuarios.index') }}" class="d-block">{{ Auth::user()->nombre }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Cobros -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
             <i class="fas fa-hand-holding-usd"></i>
              <p>
                Cobros
               
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link active">
                  <i class="fas fa-money-bill-alt"></i>
                  <p>Cobro</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('corte.index') }}" class="nav-link active">
                  <i class="fas fa-cash-register"></i>
                  <p>Corte de caja</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('Ingresos.index') }}" class="nav-link active">
                  <i class="fas fa-dollar-sign"></i>
                  <p>Ingresos</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('adeudos.index') }}" class="nav-link active">
                  <i class="fab fa-angular"></i>
                  <p>Adeudos</p>
                </a>
              </li>
             
            </ul>

          </li>
    <!-- FIN Cobros -->

    <!-- Padrón-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
             <i class="fas fa-users"></i>
              <p>
                Padron
               
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('padron.index') }}" class="nav-link active">
                  <i class="fas fa-list-ol"></i>
                  <p>Listar</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('padron.nuevo') }}" class="nav-link active">
                  <i class="fas fa-user-plus"></i>
                  <p>Registrar Ejidatario</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('padron.todo') }}" class="nav-link active">
                  <i class="fas fa-desktop"></i>
                  <p>Mostrar todo</p>
                </a>
              </li>
             
            </ul>

          </li>
    <!-- FIN padrón -->


 <!-- Canaleros -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
             <i class="fas fa-male"></i>
              <p>
                  Canaleros
               
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('canaleros.index') }}" class="nav-link active">
                  <i class="fas fa-list-ol"></i>
                  <p>Listar</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('canaleros.nuevo') }}" class="nav-link active">
                  <i class="fas fa-user-plus"></i>
                  <p>Registrar Canalero</p>
                </a>
              </li>
                <li class="nav-item">
                <a href="{{ route('libro.index') }}" class="nav-link active">
                  <i class="fas fa-book"></i>
                  <p>Libros</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('hoja.index') }}" class="nav-link active">
                  <i class="fas fa-print"></i>
                  <p>Imprimir Hoja</p>
                </a>
              </li>
             
            </ul>
          </li>
 <!-- FIN Canaleros -->

 <!-- Estadísticas -->
 <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
             <i class="fas fa-chart-line"></i>
              <p>
                  Estadísticas
              
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('graficas.index')}}" class="nav-link active">
                  <i class="fas fa-chart-pie"></i>
                  <p>Gráficas</p>
                </a>
              </li>
             
            </ul>
          </li>
 <!-- FIN Estadísticas -->


 <!-- Créditos -->
 <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
            <i class="far fa-credit-card"></i>
              <p>
                  Créditos
              
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('creditos.solicitud')}}" class="nav-link active">
                <i class="fab fa-wpforms"></i>
                  <p>Solicitud</p>
                </a>
              </li>
             
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('creditos.aprobar')}}" class="nav-link active">
                <i class="fas fa-clipboard-check"></i>
                  <p>Aprobación</p>
                </a>
              </li>
             
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('creditos.abonos')}}" class="nav-link active">
                <i class="far fa-money-bill-alt"></i>
                  <p>Abonos</p>
                </a>
              </li>
             
            </ul>

          </li>
 <!-- FIN Créditos -->

 <!-- Cerrar Sesión -->
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                       <i class="fas fa-sign-out-alt"></i>
                           <form  action="{{ route('logout') }}" method="POST" style="display: inline;">
                                        @csrf
                                   <p><button style="background: none; border: 0; color: #FFFFFF">Cerrar sesión</button></p>
                            </form> 
                    </a>
                </li>
            </ul>
  <!-- FIN-Cerrar Sesión -->

         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <div class="content">
      @yield('content')
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- 4cuadros -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <p>Usuarios</p>
      <a href="{{ route('usuarios.registry') }}" class="nav-link bg-dark">Nuevo Usuario</a>
      <a href="{{ route('usuarios.index') }}" class="nav-link bg-dark">Listar Usuarios</a>
      <div class="dropdown-divider"></div>
      <p>Historicos</p>
      <a href="{{ route('h1.index') }}" class="nav-link bg-dark">H1</a>
      <a href="{{ route('h2.index') }}" class="nav-link bg-dark">H2</a>
      <a href="{{ route('h3.index') }}" class="nav-link bg-dark">H3</a>

    </div>
  </aside>
  <!--Fin 4cuadros -->

      

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>


</body>
</html>
