<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Barryvdh\DomPDF\Facade as PDF;
use App\TotalCorte;
use Carbon\Carbon;
use App\Cobros;
use App\corte;
use DB;


class CorteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //
        $fecha = Carbon::now();
        $fechaActual = $fecha->format('d-m-Y');

     
        $sumaImportes = DB::table('corte')->sum('importe');
       

        $datosCorte = Corte::paginate(15);
        return view('corte.index',compact('datosCorte','fechaActual','sumaImportes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $Total=request()->except('_token');
         TotalCorte::insert($Total);

          DB::table('corte')->truncate();
        
         return redirect('/corte')->with('Mensaje','¡Corte de caja realizado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\corte  $corte
     * @return \Illuminate\Http\Response
     */
    public function show(corte $corte)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\corte  $corte
     * @return \Illuminate\Http\Response
     */
    public function edit(corte $corte)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\corte  $corte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, corte $corte)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\corte  $corte
     * @return \Illuminate\Http\Response
     */
    public function destroy(corte $corte)
    {
        //
    }

    public function pdf()
    {


        $fecha = Carbon::now();
        $fechaActual = $fecha->format('d-m-Y');

        $sumaImportes = DB::table('corte')->sum('importe');

        $datosCorte=Corte::all();
        $pdf = PDF::loadView('corte.pdf', compact('datosCorte','fechaActual','sumaImportes'));

        return $pdf->download('corte_de_caja.pdf');


    }

    public function verCortes()
    {
       

        $datosCorte = TotalCorte::all();
       
      
        return view('corte.verCortes',compact('datosCorte'));

    }
}
