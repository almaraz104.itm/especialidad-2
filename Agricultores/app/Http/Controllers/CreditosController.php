<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreditosStoreRequest;
use Illuminate\Http\Request;
use App\Creditos;
use App\Creditos_abonos;
use DB;

class CreditosController extends Controller
{
    public function index() {

        return view('creditos\forlulario');

    }

    public function aprobarCreditos(Request $request){

        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $credito = Creditos::buscarpor($tipo, $buscar)->paginate(20)->appends($variablesUrl);

       /// $credito=Creditos::orderBy('id','DESC')->paginate();

        return view('creditos\aprobar',compact('credito'));


    }

    public function store(CreditosStoreRequest $request){

        $datosCredito=request()->except('_token');
             Creditos::insert([
            'nombre' =>$datosCredito['nombre'], 
            'tarjeta' => $datosCredito['tarjeta'],
            'telefono' => $datosCredito['telefono'],
            'calle' => $datosCredito['calle'],
            'noexterior' => $datosCredito['noexterior'],
            'nointerior' => $datosCredito['nointerior'],
            'cpostal' => $datosCredito['cpostal'],
            'municipio' => $datosCredito['municipio'],
            'estado' => $datosCredito['estado'],
            'cpostal' => $datosCredito['cpostal'],
            'deuda' => '0',
            'ahorro' => '0',
         
        ]);

        return redirect('/creditos')->with('Mensaje','¡Solicitud registrada exitosamente!');


        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Creditos  $adeudos
     * @return \Illuminate\Http\Response
     */


    public function abonos(){

        $credito=Creditos::orderBy('id','DESC')->paginate();

        return view('creditos\abonos',compact('credito'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $Adeudo = DB::table('creditos')
                    ->where('id', '=', $id)
                    ->get();
        return view('creditos\editar',compact('Adeudo'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Recibo los datos del formulario 
         $datosAdeudo=request()->except(['_token','_method']);

         //Realizo la resta del importe y del abono 
         $adeudo = $request->get('deuda');
         $abono = $request->get('abono');
         $importe = ($adeudo -  $abono);

        if ($importe == 0) {

            DB::table('creditos')
                            ->where('id', '=', $id)
                            ->update([
                     'deuda' => $importe,]);
            Creditos::destroy($id); 
            return redirect('abonos')->with('Mensaje','¡Crédito pagado en su totalidad!');

 
            }

            if($importe < 0){

                $aux=DB::table('creditos')->select('ahorro')->where('id','=',$id)->first()->ahorro;

                $aux2=intval($aux);

                DB::table('creditos')
                ->where('id', '=', $id)
                ->update([
                'deuda' =>  0,]);
                

                DB::table('creditos')
                ->where('id', '=', $id)
                ->update([
         'ahorro' =>  ($aux2-$importe),]);

return redirect('abonos')->with('Mensaje','¡Crédito pagado en su totalidad! Y cuenta con ahorro');

            }

            if($importe>0){
                //Actualizo solo el importe 
              DB::table('creditos')
                            ->where('id', '=', $id)
                            ->update([
                     'deuda' => $importe,
                ]);


               return redirect('abonos')->with('Mensaje','¡Abono registrado exitosamente!');

            }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function edit2($id)
    {
        //
       $Adeudo = DB::table('creditos')
                    ->where('id', '=', $id)
                    ->get();
        return view('creditos\pagare',compact('Adeudo'));
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function update2(Request $request, $id)
    {
        //Recibo los datos del formulario 
         $datosAdeudo=request()->except(['_token','_method']);
         $ahorro=$request->get('ahorro');
         $adeudo = $request->get('deuda');
         $abono = $request->get('abono');

         $importe=($ahorro-$abono);

         if($importe>0){


            DB::table('creditos')
                            ->where('id', '=', $id)
                            ->update([
                     'ahorro' => $importe,
                ]);

            return redirect('abonos')->with('Mensaje','Sus ahorros han cubierto el prestamo');

         }

         if($importe<0){
            $auxiliar=($importe)*-1;

            DB::table('creditos')
                            ->where('id', '=', $id)
                            ->update([
                     'deuda' => $auxiliar+$adeudo,
                ]);

                DB::table('creditos')
                            ->where('id', '=', $id)
                            ->update([
                     'ahorro' => 0,
                ]);

            return redirect('abonos')->with('Mensaje','Crédito aprobado');

         }

         





           
            

    }

      public function destroy($id)
    {
        //
        Creditos::destroy($id);
        return redirect('/aprobar')->with('Mensaje','¡Solicitud cancelada exitosamente!');
    }



}
