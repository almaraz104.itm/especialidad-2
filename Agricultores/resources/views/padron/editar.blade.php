@extends('admin.layout')

@section('content')
   <br/>
@foreach($padron as $item)
<form method="POST" action="{{route('padron.update',$item->id)}}">
  @csrf
  {{method_field('PATCH')}}

<div class="container bg-white shadow p-1" >

   <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>Editar ejidatario: {{$item->nombre  }}</strong></h4></div>
    
   <br/>

     <div class="row justify-content-center ">
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Nombre</span>
               <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':''}}" name="nombre" id="nombre" value="{{ $item->nombre }}" required>
               <span class="focus-input100"></span>
                @error('nombre')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Tarjeta</span>
               <input type="text" class="form-control {{$errors->has('tarjeta')?'is-invalid':''}}" name="tarjeta" id="tarjeta" value="{{ $item->tarjeta }}" required>
               <span class="focus-input100"></span>
               @error('tarjeta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Cuenta</span>
               <input type="text" class="form-control {{$errors->has('cuenta')?'is-invalid':''}}" name="cuenta" id="cuenta" value="{{ $item->cuenta }}" required>
               <span class="focus-input100"></span>
               @error('cuenta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Municipio</span>
                <input type="text" class="form-control {{$errors->has('municipio')?'is-invalid':''}}" name="municipio" id="municipio" value="{{ $item->municipio }}" required>
               <span class="focus-input100"></span>
               @error('municipio')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
              
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Ejido</span>
                <input type="text" class="form-control {{$errors->has('ejido')?'is-invalid':''}}" name="ejido" id="ejido" value="{{$item->ejido}}" required>
                <span class="focus-input100"></span>
                @error('ejido')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Sección</span>
                <input type="text" class="form-control {{$errors->has('seccion')?'is-invalid':''}}" name="seccion" id="seccion" value="{{ $item->seccion }}">
                <span class="focus-input100"></span>
                @error('seccion')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >PRO</span>
                <input type="text" class="form-control {{$errors->has('pro')?'is-invalid':''}}" name="pro" id="pro" value="{{ $item->pro }}">
                <span class="focus-input100"></span>
                @error('pro')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Superficie física</span>
                <input type="text" class="form-control {{$errors->has('sup_fis')?'is-invalid':''}}" name="sup_fis" id="sup_fis" value="{{ $item->sup_fis }}" step="any">
                <span class="focus-input100"></span>
                @error('sup_fis')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >No Ejido</span>
                <input type="text" class="form-control {{$errors->has('ejido_no')?'is-invalid':''}}" name="ejido_no" id="ejido_no" value="{{ $item->ejido_no }}">
                <span class="focus-input100"></span>
                @error('ejido_no')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-4 col-sm-4 col-xs-4">
                <span class="help-block text-muted small-font" >Sistema</span>
               <input type="text" class="form-control {{$errors->has('sistema')?'is-invalid':''}}" name="sistema" id="sistema" value="{{ $item->sistema }}" required>
               <span class="focus-input100"></span>
               @error('sistema')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
           <div class="col-md-5 col-sm-5 col-xs-5">
                <span class="help-block text-muted small-font" >Canalero</span>
                <input type="text" class="form-control {{$errors->has('canalero')?'is-invalid':''}}" name="canalero" id="canalero" value="{{ $item->canalero }}" required>
               <span class="focus-input100"></span>
               @error('canalero')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
      </div>
@endforeach
@foreach($padron as $item)
      <div class="row justify-content-center">
           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >S_CTA</span>
                <input type="text" class="form-control {{$errors->has('s_cta')?'is-invalid':''}}" name="s_cta" id="s_cta" value="{{$item->s_cta}}">
                <span class="focus-input100"></span>
                @error('s_cta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >DOC</span>
                <input type="text" class="form-control {{$errors->has('doc')?'is-invalid':''}}" name="doc" id="doc" value="{{ $item->doc }}">
                  @error('doc')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                  @enderror
           </div>
           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >No</span>
                <input type="text" class="form-control {{$errors->has('no')?'is-invalid':''}}" name="no" id="no" value="{{ $item->no }}">
                <span class="focus-input100"></span>
                @error('no')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Fecha</span>
                <input type="date" class="form-control {{$errors->has('fecha')?'is-invalid':''}}" name="fecha" id="fecha" value="{{ $item->fecha }}">
                <span class="focus-input100"></span>
                @error('fecha')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

             <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Tarjeta_ant</span>
                <input type="text" class="form-control {{$errors->has('tarjeta_ant')?'is-invalid':''}}" name="tarjeta_ant" id="tarjeta_ant" value="{{ $item->tarjeta_ant }}">
                <span class="focus-input100"></span>
                @error('tarjeta_ant')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Usipad</span>
                <input type="text" class="form-control {{$errors->has('usipad')?'is-invalid':''}}" name="usipad" id="usipad" value="{{ $item->usipad }}">
                <span class="focus-input100"></span>
                @error('usipad')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Cta_Sipad</span>
                <input type="text" class="form-control {{$errors->has('cta_sipad')?'is-invalid':''}}" name="cta_sipad" id="cta_sipad" value="{{ $item->cta_sipad }}">
                <span class="focus-input100"></span>
                @error('cta_sipad')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>          
      </div>

      <div class="row justify-content-center">
         
           <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Superficie física</span>
                <input type="text" class="form-control {{$errors->has('sup_fisica')?'is-invalid':''}}" name="sup_fisica" id="sup_fisica" value="{{ $item->sup_fisica }}">
                 @error('sup_fisica')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Superficie riego</span>
                <input type="text" class="form-control {{$errors->has('sup_riego')?'is-invalid':''}}" name="sup_riego" id="sup_riego" value="{{ $item->sup_riego }}">
                <span class="focus-input100"></span>
                 @error('sup_riego')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Sección original</span>
                <input type="text" class="form-control {{$errors->has('sec_orig')?'is-invalid':''}}" name="sec_orig" id="sec_orig" value="{{ $item->sec_orig }}">
                <span class="focus-input100"></span>
                 @error('sec_orig')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>   
      </div>



<br/>
               <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('padron.index') }}" class="btn btn-link">
                                  <h1> <i class="fas fa-times"></i></h1>  
                                 </a>

                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link ">
                                    <h1> <i class="far fa-save"></i></h1>
                                   
                                </button>

                            </div>

              </div>
                 <br/>
</div>

  
</form>
@endforeach
@endsection