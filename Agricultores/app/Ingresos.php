<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingresos extends Model
{
    //
    protected $table = 'ingresos';
    protected $fillable = [
        'factura',
        'fecha',
        'tarjeta',
        'nombre',
        'ciclo',
        'cuenta',
        'seccion', 
        'ejido',
        'municipio',  
        'cultivo', 
        'superficie', 
        'concepto', 
        'tipo',
        'unidad', 
        'cuota2', 
        'riegos',
        'cantidad',
        'total',
        
    ];

      public function scopeBuscarpor($query, $tipo, $buscar){
        if(($tipo) && ($buscar)){
            return $query->where($tipo,'like',"%$buscar%");
        }
    }
}
