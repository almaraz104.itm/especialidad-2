@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);padding-top: 1px;">
                   Concepto
                   <div class="text-right">
                    
                        <a  href="{{ route('concepto.nuevo') }}">
                            <button class="btn pmd-btn-fab pmd-ripple-effect btn-info" type="button"><i class="fas fa-plus-circle"></i></button>
                        </a>
                    
                   </div>
                </div>

                <div class="card-body">

<table class="table table-hover table-responsive-lg">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Clave</th>
      <th scope="col">Descripción</th>
      <th scope="col">Unidad</th>
      <th scope="col">Precio</th>
      <th scope="col">Tipo</th>
      <th colspan="3">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
     @foreach($conceptos as $item)
                        <tr>
                          
                            <td>{{$item->clave}}</td>
                              <td>{{$item->descripcion}}</td>
                                <td>{{$item->unidad}}</td>
                                  <td>{{$item->precio}}</td>
                                    <td>{{$item->tipo}}</td>
                                      <td width="10px">
                       
                                          <a href="{{ route('concepto.edit',$item->id) }}" class="btn btn-sm btn-default" >
                                            <i class="fas fa-edit"></i>
                                          </a>

                                          </td>

                                          <td width="10px">
                            
                                              <form method="post" action="{{route('concepto.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este registrp?');">
                                                      <i class="far fa-trash-alt"></i>    
                                                  </button>
                                              </form>   
                        
                                           </td>
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>

{{--Paginación--}}
    <div class="row justify-content-center responsive">                       
       {{$conceptos->onEachSide(2)->links()}}                  
    </div>
                   


                   

                </div>
            </div>
        </div>
    </div>
</div>


@endsection