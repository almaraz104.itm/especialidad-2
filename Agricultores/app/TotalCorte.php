<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalCorte extends Model
{
    //
    protected $table = 'totalcorte';
    protected $fillable = [
        'total',
        'created_at',    
    ];
}
