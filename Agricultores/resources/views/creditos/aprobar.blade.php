@extends('admin.layout')

@section('content')


<!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->


<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">

                
                
                 
<!-- Filtros -->
<nav class="navbar navbar-light float-right" style="background-color:rgba(0, 120, 155,  0.6);">
  <h2 class="text-white"><strong>Aprobación de créditos</strong></h2>

  <form class="form-inline" >

      <select name="tipo" class="form-control mr-sm-2"  required>
        <option selected>Buscar por...</option>
        <option>Nombre</option>
        <option>Tarjeta</option>
      </select>

    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Buscar" required>
    <button class="btn btn-link" type="submit" >
      <h2 style="margin-top: 10px; margin-left: -10px;"><i class="fas fa-search text-white"></i></h2>
    </button> 
    
    <a href="{{ route('creditos.aprobar') }}" style="margin-top: 10px; margin-left: 5px"><h2><i class="fas fa-sync-alt text-white"></i></h2></a>

  </form>

</nav>
<!-- Fin Filtros -->

<div class="card-body">
 <table class="table table-hover table-responsive-lg ">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Nombre</th>
      <th scope="col">Tarjeta</th>
      <th colspan="3">Acciones</th>
      
      
    </tr>
  </thead>
  <tbody>
     @foreach($credito as $item)
                        <tr>
                          
                            <td>{{$item->nombre}}</td>
                              <td>{{$item->tarjeta}}</td>
                                

                                      <td width="10px">
                            
                                          <a href="{{route('creditos.show',$item->id)}}"  >
                                          <i class="fas fa-check-circle fa-2x"></i>
                                           
                                          </a>
                       
                                      </td>

                                      <td width="10px">
                            
                                              <form method="post" action="{{route('solicitud.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-link" onclick="return confirm('¿Seguro desea cancelar esta solicitud?');">
                                                      <i class="fas fa-trash-alt fa-2x"></i>    
                                                  </button>
                                              </form>  
                        
                                           </td>
                                       


                                      
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>

 
                 

                    {{--Paginación--}}

                     <div class="row justify-content-center responsive">                       
                        {{$credito->onEachSide(2)->links()}}                  
                     </div>

                </div>
            </div>
        </div>
    </div>
</div>

  {{-- Boton arriba flotante --}}
  <a href="#" class="btn btn-info back-to-top">
              <i class="fas fa-chevron-up"></i>
  </a>


@endsection