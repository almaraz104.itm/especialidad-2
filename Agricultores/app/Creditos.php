<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Creditos extends Model
{
    protected $fillable=['nombre','tarjeta','telefono','calle','noexterior','nointerior','cpostal','municipio','estado','deuda','ahorro','_token'];

    public function scopeBuscarpor($query, $tipo, $buscar){
        if(($tipo) && ($buscar)){
            return $query->where($tipo,'like',"%$buscar%");
        }
    }

}

