<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuariosUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username' => ['required','max:25'],
            'nombre' => ['required','string','min:3','max:25'],
            'apaterno' => ['required','string','min:1','max:25'],
            'amaterno' => ['required','string','min:1','max:25'],
            'telefono' => ['required','numeric', 'digits_between:10,13'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ];
    }
}
