@extends('admin.layout')

@section('content')

 <!-- Muestro mensaje de exito -->
        @if(Session::has('Mensaje'))
           <div class="alert alert-success" role="alert">
               {{Session::get('Mensaje')}}
           </div>
        @endif
<!-- Fin mensaje-->

<br/>
<form method="POST" action="{{route('padron.store')}}">
  @csrf

<div class="container bg-white shadow p-1" >

   <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Registrar nuevo ejidatario') }}</strong></h4></div>
    
   <br/>

     <div class="row justify-content-center ">
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Nombre</span>
               <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':''}}" name="nombre" id="nombre" value="{{ old('nombre') }}" required>
               <span class="focus-input100"></span>
                @error('nombre')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Tarjeta</span>
               <input type="text" class="form-control {{$errors->has('tarjeta')?'is-invalid':''}}" name="tarjeta" id="tarjeta" value="{{ old('tarjeta') }}" required>
               <span class="focus-input100"></span>
               @error('tarjeta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Cuenta</span>
               <input type="text" class="form-control {{$errors->has('cuenta')?'is-invalid':''}}" name="cuenta" id="cuenta" value="0" required>
               <span class="focus-input100"></span>
               @error('cuenta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
          </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Municipio</span>
               <select class="form-control" name="municipio">
                    <option selected>--- Selecciona una opción ---</option>
                    <option >Álvaro Obregón</option>
                    <option >Tarimbaro</option>
                </select>
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Ejido</span>
                <input type="text" class="form-control {{$errors->has('ejido')?'is-invalid':''}}" name="ejido" id="ejido" value="{{ old('ejido') }}" required>
                <span class="focus-input100"></span>
                @error('ejido')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Sección</span>
                <input type="text" class="form-control {{$errors->has('seccion')?'is-invalid':''}}" name="seccion" id="seccion" value="0">
                <span class="focus-input100"></span>
                @error('seccion')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >PRO</span>
                <input type="text" class="form-control {{$errors->has('pro')?'is-invalid':''}}" name="pro" id="pro" value="0">
                <span class="focus-input100"></span>
                @error('pro')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Superficie física</span>
                <input type="text" class="form-control {{$errors->has('sup_fis')?'is-invalid':''}}" name="sup_fis" id="sup_fis" value="0" step="any">
                <span class="focus-input100"></span>
                @error('sup_fis')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >No Ejido</span>
                <input type="text" class="form-control {{$errors->has('ejido_no')?'is-invalid':''}}" name="ejido_no" id="ejido_no" value="0">
                <span class="focus-input100"></span>
                @error('ejido_no')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-4 col-sm-4 col-xs-4">
                <span class="help-block text-muted small-font" >Sistema</span>
                <select class="form-control" name="sistema">
                    <option selected>--- Selecciona una opción ---</option>
                    <option >Gravedad</option>
                    <option >Pozo</option>
                </select>
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-5 col-sm-5 col-xs-5">
                <span class="help-block text-muted small-font" >Canalero</span>
                <select class="form-control" name="canalero">
                    <option selected>--- Selecciona una opción ---</option>
                     @foreach($canaleros as $item)
                       <option>{{$item->nombre}}</option>
                     @endforeach
                </select>
                <span class="focus-input100"></span>
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >S_CTA</span>
                <input type="text" class="form-control {{$errors->has('s_cta')?'is-invalid':''}}" name="s_cta" id="s_cta" value="0">
                <span class="focus-input100"></span>
                @error('s_cta')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >DOC</span>
                <input type="text" class="form-control {{$errors->has('doc')?'is-invalid':''}}" name="doc" id="doc" value="{{ old('doc') }}">
                  @error('doc')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                  @enderror
           </div>
           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >No</span>
                <input type="text" class="form-control {{$errors->has('no')?'is-invalid':''}}" name="no" id="no" value="0">
                <span class="focus-input100"></span>
                @error('no')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Fecha</span>
                <input type="date" class="form-control {{$errors->has('fecha')?'is-invalid':''}}" name="fecha" id="fecha" value="{{ old('fecha') }}">
                <span class="focus-input100"></span>
                @error('fecha')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

             <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Tarjeta_ant</span>
                <input type="text" class="form-control {{$errors->has('tarjeta_ant')?'is-invalid':''}}" name="tarjeta_ant" id="tarjeta_ant" value="{{ old('tarjeta_ant') }}">
                <span class="focus-input100"></span>
                @error('tarjeta_ant')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Usipad</span>
                <input type="text" class="form-control {{$errors->has('usipad')?'is-invalid':''}}" name="usipad" id="usipad" value="{{ old('usipad') }}">
                <span class="focus-input100"></span>
                @error('usipad')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-1 col-sm-1 col-xs-1">
                <span class="help-block text-muted small-font" >Cta_Sipad</span>
                <input type="text" class="form-control {{$errors->has('cta_sipad')?'is-invalid':''}}" name="cta_sipad" id="cta_sipad" value="0">
                <span class="focus-input100"></span>
                @error('cta_sipad')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>          
      </div>

      <div class="row justify-content-center">
         
           <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Superficie física</span>
                <input type="text" class="form-control {{$errors->has('sup_fisica')?'is-invalid':''}}" name="sup_fisica" id="sup_fisica" value="0">
                 @error('sup_fisica')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

           <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Superficie riego</span>
                <input type="text" class="form-control {{$errors->has('sup_riego')?'is-invalid':''}}" name="sup_riego" id="sup_riego" value="0">
                <span class="focus-input100"></span>
                 @error('sup_riego')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>

            <div class="col-md-2 col-sm-2 col-xs-2">
                <span class="help-block text-muted small-font" >Sección original</span>
                <input type="text" class="form-control {{$errors->has('sec_orig')?'is-invalid':''}}" name="sec_orig" id="sec_orig" value="0">
                <span class="focus-input100"></span>
                 @error('sec_orig')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                      </span>
                @enderror
           </div>   
      </div>



<br/>
               <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('padron.index') }}" class="btn btn-link">
                                   
                                         <h1> <i class="fas fa-times"></i></h1> 
                                    
                                 </a>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                     <h1> <i class="far fa-save"></i></h1> 
                                </button>
                            </div>

              </div>
                 <br/>
</div>

  
</form>

@endsection