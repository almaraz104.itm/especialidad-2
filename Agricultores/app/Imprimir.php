<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imprimir extends Model
{
    //
    protected $table = 'hoja';
     protected $fillable = [
        'nombre',
        'tarjeta',
        'cpl',
        'tsl',
        'ra',
        'pco',
        'sr',
        'sup_fis',
        'p_dren',
        's_dren',
        'cultivo',
        'hf',
        'p1',
        'r1',
        't1',
        'p2',
        'r2',
    ];
}
