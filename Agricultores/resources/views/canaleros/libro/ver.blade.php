@extends('admin.layout')

@section('content')

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow p-1">

			@foreach($libro as $item)
                <div class="card-header h5 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);">
                    Libro de fecha: {{$item->fecha}}

                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                        <th scope="row">Nombre:</th>
                        <td>{{$item->usuario}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Tarjeta:</th>
                        <td>{{$item->tarjeta}}</td>
                        </tr>
                        <tr>
                        <th scope="row">CPL:</th>
                        <td>{{$item->cpl}}</td>
                        </tr>
                        <tr>
                        <th scope="row">TSL:</th>
                        <td>{{$item->tsl}}</td>
                        </tr>
                        <tr>
                        <th scope="row">RA:</th>
                        <td>{{$item->ra}}</td>
                        </tr>
                        <tr>
                        <th scope="row">PCO:</th>
                        <td>{{$item->pco}}</td>
                        </tr>
                        <tr>
                        <th scope="row">SR:</th>
                        <td>{{$item->sr}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Superfice Física:</th>
                        <td>{{$item->sup_fis}}</td>
                        </tr>
                        <tr>
                        <th scope="row">P_DREN:</th>
                        <td>{{$item->p_dren}}</td>
                        </tr>
                        <tr>
                        <th scope="row">S_DREN:</th>
                        <td>{{$item->s_dren}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Cultivo:</th>
                        <td>{{$item->cultivo}}</td>
                        </tr>
                        <tr>
                        <th scope="row">HF:</th>
                        <td>{{$item->hf}}</td>
                        </tr>
                        <tr>
                        <th scope="row">P1:</th>
                        <td>{{$item->p1}}</td>
                        </tr>
                        <tr>
                        <th scope="row">R1:</th>
                        <td>{{$item->r1}}</td>
                        </tr>
                        <tr>
                        <th scope="row">T1:</th>
                        <td>{{$item->t1}}</td>
                        </tr>
                        <tr>
                        <th scope="row">P2:</th>
                        <td>{{$item->p2}}</td>
                        </tr>
                        <tr>
                        <th scope="row">R2:</th>
                        <td>{{$item->r2}}</td>
                        </tr>    
                    </table>

                      <div class="text-right">
                          <a href="{{ route('libro.index')}}" class="btn btn-sm btn-link">
                            <h1> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
             
            </div>
            @endforeach
        </div> 

    </div>



    
</div>



@endsection