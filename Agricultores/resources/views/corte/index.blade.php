@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);padding-top: 1px;">
                   Corte de caja al dia : {{ $fechaActual }}
                   <div class="text-right">
                      <a href="{{ route('corte.verCortes') }}" class="btn btn-link">
                       <h2> <i class="far fa-eye text-white"></i></h2>
                      </a>
                   </div>
                </div>

                <div class="card-body">
                   <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th scope="col">Cooperaciones</th>
                            <th scope="col">Servicio_Riegos</th>
                            <th scope="col">Maquinaria</th>
                            <th scope="col">S_drenaje</th>
                            <th scope="col">Importe</th>
                        </tr>

                    </thead>
                    <tbody>
                      <form action="{{ route('corte.store') }}" method="POST">
                         @csrf
                        @foreach($datosCorte as $item)
                        <tr>
                       
                            <td>
                              <input type="text"  value=" {{$item->cooperaciones}}" style="border:0" readonly>
                            </td>

                            <td>
                               <input type="text" value=" {{$item->servicio}}" style="border:0" readonly>
                            </td>

                            <td>
                               <input type="text" value=" {{$item->maquinaria}}" style="border:0" readonly>
                            </td>

                            <td>
                               <input type="text"  value=" {{$item->drenaje}}" style="border:0" readonly>
                            </td>

                            <td>
                                 <input type="text" id="importe" value=" {{$item->importe}}" class="form-control" style="border:0"  readonly>
                            </td>

                           
                        </tr>


                          
                        @endforeach

                            
          
                      <button type="submit" class=" btn btn-outline-warning btn-block" onclick="return confirm('Antes de realizar el corte de caja, puede imprimir el reporte del día. ¿Desea continuar?');">Corte</button>

                    </tbody>
                       
                          

                   </table>
                   {{$datosCorte->render()}}


                </div>
                
            </div>
        </div>

  <div class="container d-flex flex-row-reverse bd-highlight">
      <div class="col-md-3">
        <div class="card">
          <div class="card-body d-flex justify-content-center align-items-center">
            TOTAL: 
            
              <input type="text" id="sumaTotal" name="total" value="  {{ $sumaImportes }}" class="form-control" style="border:0"  readonly>
       
          </div>
        </div>
      </div>  
  </div>


                        </form>

         <a href="{{ route('descargar.pdf') }}" class="d-flex justify-content-center">
                 <h1><i class="far fa-file-pdf"></i></h1>              
            </a>
    </div>
    <span class="d-flex justify-content-center" style="font-size: 10px; margin-top: -10px">Generar-PDF</span>  
    <br/> 

</div>


@endsection