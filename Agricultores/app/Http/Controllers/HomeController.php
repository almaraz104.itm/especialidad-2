<?php

namespace App\Http\Controllers;

use App\Http\Requests\CobrosStoreRequest;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Ejidos;
use App\Cobros;
use App\Corte;
use App\Ingresos;
use App\Concepto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        
         //Mando datos al Modal
        $conceptos = Concepto::all();
        $ejidos = Ejidos::all();
        return view('home',compact('conceptos','ejidos'));

        
    }

    public function store(CobrosStoreRequest $request)
    {

        $datosCobro=request()->except('_token');
             Cobros::insert([
            'factura' =>$datosCobro['factura'], 
            'nombre' => $datosCobro['nombre'],
            'tarjeta' => $datosCobro['tarjeta'],
            'no' => $datosCobro['no'],
            'superficie' => $datosCobro['superficie'],
            'fecha' => $datosCobro['fecha'],
            'clave' => $datosCobro['clave'],
            'acuerdo' => $datosCobro['acuerdo'],
            'tipo' => $datosCobro['tipo'],
            'year' => $datosCobro['year'],
            'caracter' => $datosCobro['caracter'],
            'cuota' => $datosCobro['cuota'],
            'cuenta' => $datosCobro['cuenta'],
            'seccion' => $datosCobro['seccion'],
            'ejido' => $datosCobro['ejido'],
            'municipio' => $datosCobro['municipio'],
            'concepto' => $datosCobro['concepto'],
            'tipo2' => $datosCobro['tipo2'],
            'ciclo' => $datosCobro['ciclo'],
            'unidad' => $datosCobro['unidad'],
            'cuota2' => $datosCobro['cuota2'],
            'riegos' => $datosCobro['riegos'],
            'cantidad' => $datosCobro['cantidad'],
            'importe' => $datosCobro['importe'],
            'cooperaciones' => $datosCobro['cooperaciones'],
            'servicio' => $datosCobro['servicio'],
            'maquinaria' => $datosCobro['maquinaria'],
            'drenaje' => $datosCobro['drenaje'],
            'total' => $datosCobro['total'], 
            
        ]);

        $datosCobro=request()->except('_token');
             Ingresos::insert([
            'factura' =>$datosCobro['factura'], 
            'fecha' => $datosCobro['fecha'],
            'tarjeta' => $datosCobro['tarjeta'],
            'nombre' => $datosCobro['nombre'],
            'ciclo' => $datosCobro['ciclo'],
            'cuenta' => $datosCobro['cuenta'],
            'seccion' => $datosCobro['seccion'],
            'ejido' => $datosCobro['ejido'],
            'municipio' => $datosCobro['municipio'],
            'cultivo' => 'NA',
            'superficie' => $datosCobro['superficie'],
            'concepto' => $datosCobro['concepto'],
            'tipo' => $datosCobro['tipo'],
            'unidad' => $datosCobro['unidad'],
            'cuota2' => $datosCobro['cuota2'],
            'riegos' => $datosCobro['riegos'],
            'cantidad' => $datosCobro['cantidad'],
            'total' => $datosCobro['total'],        
        ]);

        $datosCobro=request()->except('_token');
             Corte::insert([
            'cooperaciones' =>$datosCobro['cooperaciones'], 
            'servicio' => $datosCobro['servicio'],
            'maquinaria' => $datosCobro['maquinaria'],
            'drenaje' => $datosCobro['drenaje'],
            'importe' => $datosCobro['total'],
                 
        ]);




      //  $datosCobro=request()->except('_token');
        //Cobros::insert($datosCobro);

        
        return redirect('/home')->with('Mensaje','¡Cobro realizado exitosamente!');

    }


    public function recibo()
    {

    //Obtengo el total de registros de la tabla
    $totalID = Cobros::count();
    //Obtengo el ultimo registro insertado
    $datosCobro = Cobros::where('id', $totalID)->first();      
                   
        $pdf = PDF::loadView('cobros.recibo', compact('datosCobro'));

         return $pdf->download('Recibo.pdf');

      
    }

    public function busqueda(Request $request){
        
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $datosPadron = Padron::buscarpor($tipo, $buscar)->appends($variablesUrl);
        return view('home',compact('datosPadron'));
        
    }




}
