@extends('admin.layout')

@section('content')



<br/>


 <div class="row justify-content-center">


  
        <div class="col-md-10 shadow p-2">
            <div class="card">
               <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Registrar Canalero') }}</strong></h4></div>

                <div class="card-body">
                    <form method="POST" action="{{route('canaleros.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':''}} " name="nombre" id="nombre" value="{{ old('nombre') }}" autofocus style="text-transform:capitalize;">  
                                <div class="invalid-feedback"> 
                                    Campo Nombre es obligatorio
                                </div>                         
                           </div>
                       </div>

                          <div class="form-group row">
                            <label for="seccion" class="col-md-4 col-form-label text-md-right">{{ __('Zona') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('seccion')?'is-invalid':''}} " name="seccion" id="seccion" value="{{ old('seccion') }}" autofocus style="text-transform:capitalize;">  
                                <div class="invalid-feedback"> 
                                    El campo Zona es obligatorio
                                </div>                         
                           </div>
                       </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Dirección') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('direccion')?'is-invalid':''}}" name="direccion" id="fsiembra" value="{{ old('direccion') }}" autofocus style="text-transform:capitalize;">
                                <div class="invalid-feedback"> 
                                    El campo Dirección es obligatorio
                                </div>             
                            </div>
                        </div>
 
                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('telefono')?'is-invalid':''}}" name="telefono" id="telefono" value="{{ old('telefono') }}"> 
                                <div class="invalid-feedback"> 
                                    El campo Teléfono es obligatorio
                                </div>            
                            </div>
                        </div>

            
                         <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('canaleros.index') }}" class="btn btn-link">
                                   
                                   <h1> <i class="fas fa-times"></i></h1> 
                                    
                                 </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                    <h1> <i class="far fa-save"></i></h1> 
                                </button>
                            </div>
                        </div>
    
                         </div>
                    </form>
                </div>
            </div>
        </div>


@endsection







