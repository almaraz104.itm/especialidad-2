@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                   Cultivos
                   <div class="text-right">
                    
                        <a  href="{{ route('cultivo.nuevo') }}">
                            <button class="btn pmd-btn-fab pmd-ripple-effect btn-info" type="button"><i class="fas fa-plus-circle"></i></button>
                        </a>
                    
                   </div>
                </div>

                <div class="card-body">

<table class="table table-hover table-responsive-lg">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Clave</th>
      <th scope="col">Cultivo</th>
      <th scope="col">Siembra</th>
      <th scope="col">Cosecha</th>
      <th scope="col">Soca</th>
      <th scope="col">Fuente</th>
      <th colspan="3">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
     @foreach($cultivos as $item)
                        <tr>
                          
                            <td>{{$item->clave}}</td>
                              <td>{{$item->cultivo}}</td>
                                <td>{{$item->fsiembra}}</td>
                                  <td>{{$item->fcosecha}}</td>
                                    <td>{{$item->fsoca}}</td>
                                     <td>{{$item->fuente_ab}}</td>
                                      <td width="10px">
                       
                                          <a href="{{ route('cultivo.edit',$item->id) }}" class="btn btn-sm btn-default" >
                                            <i class="fas fa-edit"></i>
                                          </a>

                                          </td>

                                          <td width="10px">
                            
                                              <form method="post" action="{{route('cultivo.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este cultivo?');">
                                                      <i class="far fa-trash-alt"></i>    
                                                  </button>
                                              </form>  
                        
                                           </td>
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>

{{--Paginación--}}
    <div class="row justify-content-center responsive">                       
       {{$cultivos->onEachSide(2)->links()}}                  
    </div>
                   


                   

                </div>
            </div>
        </div>
    </div>
</div>


@endsection