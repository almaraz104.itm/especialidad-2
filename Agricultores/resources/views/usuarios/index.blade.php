@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);padding-top: 1px;">
                   Usuarios
                   <div class="text-right">
                    
                        <a  href="{{ route('usuarios.registry') }}">
                            <button class="btn pmd-btn-fab pmd-ripple-effect btn-info" type="button"><i class="fas fa-user-plus"></i></button>
                        </a>
                    
                   </div>
                </div>

                <div class="card-body">
                   <table class="table table-hover">
                    <thead>
                        <tr>
                            
                            <th>Nombre</th>
                            <th colspan="3">&nbsp;</th>
                        </tr>

                    </thead>
                    <tbody>
                        @foreach($usuarios as $item)
                        <tr>
                       
                            <td>{{$item->nombre}}</td>

                            <td width="10px">
                       
                                <a href="{{route('usuarios.show',$item->id)}}" class="btn btn-sm btn-default">
                                  <i class="far fa-eye"></i>
                                </a>
             
                            </td>

                            <td width="10px">
                            
                                <a href="{{ route('usuarios.edit',$item->id) }}" class="btn btn-sm btn-default" >
                                  <i class="fas fa-edit"></i>
                                </a>
                             
                           
                            </td>

                             <td width="10px">
                            
                                <form method="post" action="{{route('usuarios.destroy',$item->id)}}">
                                      @csrf
                                        {{method_field('DELETE')}}
                                           <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este usuario?');">
                                                 <i class="far fa-trash-alt"></i>    
                                           </button>
                                </form>  
                        
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                       

                   </table>
                   {{$usuarios->render()}}

                </div>
            </div>
        </div>
    </div>
</div>


@endsection