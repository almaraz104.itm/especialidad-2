<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class corte extends Model
{
    //
    protected $table = 'corte';
    protected $fillable = [
        'cooperaciones',
        'servicios',
        'maquinaria',
        'importe',
        'created_at',  
        'updated_at',   
    ];
}
