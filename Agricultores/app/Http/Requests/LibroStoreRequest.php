<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LibroStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          return [
            //
            'usuario' => ['required','string'],
            'tarjeta' => ['required','numeric'],
            'cpl' => ['required','numeric'],
            'tsl' => ['required','numeric'],
            'ra' => ['required','numeric'],
            'pco' => ['required','numeric'],
            'sr' => ['required','numeric'],
            'sup_fis' => ['required','numeric'],
            'p_dren' => ['required','numeric'],
            's_dren' => ['required','numeric'],
            'cultivo' => ['required','string'],
            'hf' => ['required','numeric'],
            'p1' => ['required','numeric'],
            'r1' => ['required','numeric'],
            't1' => ['required','numeric'],
            'p2' => ['required','numeric'],
            'r2' => ['required','numeric'],
            'fecha' => ['required'],

            
        ];
    }
}
