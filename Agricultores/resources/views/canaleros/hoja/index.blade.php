
@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-info" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->

<br/>

<form method="POST" action="{{ route('hoja.store') }}">
  @csrf

<div class="container bg-white shadow p-2" >

    <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Datos de la hoja') }}</strong></h4></div>
        
   <br/>

     <div class="row justify-content-center ">
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Nombre</span>
               <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':''}}" name="nombre" value=".">
               <span class="focus-input100"></span>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Tarjeta</span>
               <input type="text" class="form-control {{$errors->has('tarjeta')?'is-invalid':''}}" name="tarjeta" value=".">
               <span class="focus-input100"></span>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >CPL</span>
               <input type="text" class="form-control {{$errors->has('rfc')?'is-invalid':''}}" name="cpl" value=".">
               <span class="focus-input100"></span>
          </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >TSL</span>
                <input type="text" class="form-control {{$errors->has('tsl')?'is-invalid':''}}" name="tsl" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >RA</span>
                <input type="text" class="form-control {{$errors->has('diascredito')?'is-invalid':''}}" name="ra" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >PCO</span>
                <input type="text" class="form-control {{$errors->has('pco')?'is-invalid':''}}" name="pco" value=".">
                <span class="focus-input100"></span>
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >SR</span>
                <input type="text" class="form-control {{$errors->has('saldo')?'is-invalid':''}}" name="sr" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >SUP_FIS</span>
                <input type="text" class="form-control {{$errors->has('sup_fis')?'is-invalid':''}}" name="sup_fis" value=".">
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P_DREN</span>
                <input type="text" class="form-control {{$errors->has('descuento')?'is-invalid':''}}" name="p_dren" value=".">
                <span class="focus-input100"></span>
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >S_DREN</span>
                <input type="text" class="form-control {{$errors->has('noexterior')?'is-invalid':''}}" name="s_dren" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >CULTIVO</span>
                <input type="text" class="form-control {{$errors->has('cultivo')?'is-invalid':''}}" name="cultivo" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >HF</span>
                <input type="text" class="form-control {{$errors->has('hf')?'is-invalid':''}}" name="hf" value=".">
                <span class="focus-input100"></span>
           </div>
      </div>

       <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P1</span>
                <input type="text" class="form-control {{$errors->has('p1')?'is-invalid':''}}" name="p1" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >R1</span>
                <input type="text" class="form-control {{$errors->has('r1')?'is-invalid':''}}" name="r1" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >T1</span>
                <input type="text" class="form-control {{$errors->has('t1')?'is-invalid':''}}" name="t1" value=".">
                <span class="focus-input100"></span>
           </div>
      </div>

       <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P2</span>
                <input type="text" class="form-control {{$errors->has('r2')?'is-invalid':''}}" name="p2" value=".">
                <span class="focus-input100"></span>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >R2</span>
                <input type="text" class="form-control {{$errors->has('colonia')?'is-invalid':''}}" name="r2" value=".">
                <span class="focus-input100"></span>
           </div>
      </div>

     

      <br/>

       <div class="row">
                            <div class="col text-center">
                              
                                  <a href="{{ route('eliminar.destroy') }}" class="btn btn-outline-danger btn-sm">
                                   
                                    {{ __('Eliminar datos') }}
                                    
                                 </a>
                                
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                    <h1> <i class="far fa-save"></i></h1> 
                                </button>
                            </div>

                            <a href="{{ route('hoja.ver') }}" class="btn btn-link" style="margin-right: 20px">
                                   
                                   <h1><i class="far fa-eye"></i></h1>
                                    
                            </a>
                             
                        </div>

    
</div>
<br/>
</form>

@endsection