@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                   Libros
                   <div class="text-right">
                    
                        <a  href="{{ route('libro.nuevo') }}">
                            <button class="btn pmd-btn-fab pmd-ripple-effect btn-info" type="button"><i class="fas fa-plus-circle"></i></button>
                        </a>
                    
                   </div>
                </div>

                <div class="card-body">

<table class="table table-hover table-responsive-lg">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Fecha</th>
      <th colspan="3">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
   @foreach($libros as $item)
                        <tr>
                          
                            <td>{{$item->fecha}}</td>
                             <td width="10px">
                                 
                                          <a href="{{route('libro.show',$item->id)}}" class="btn btn-sm btn-default">
                                            <i class="far fa-eye"></i>
                                          </a>
                       
                                      </td>

                                          <td width="10px">
                            
                                              <form method="post" action="{{route('libro.destroy',$item->id)}}">
                                                 @csrf
                                                 {{method_field('DELETE')}}
                                                  <button class="btn btn-sm btn-default" onclick="return confirm('¿Seguro desea eliminar este libro?');">
                                                      <i class="far fa-trash-alt"></i>    
                                                  </button>
                                              </form>  
                        
                                           </td>
                                       
                        </tr>
                       
                 


     @endforeach
  </tbody>
</table>

               


                   

                </div>
            </div>
        </div>
    </div>
</div>


@endsection