<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImprimirStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre' => ['required','string'],
            'tarjeta' => ['required','string'],
            'cpl' => ['required','string'],
            'tsl' => ['required','string'],
            'ra' => ['required','string'],
            'pco' => ['required','string'],
            'sr' => ['required','string'],
            'sup_fis' => ['required','string'],
            'p_dren' => ['required','string'],
            's_dren' => ['required','string'],
            'cultivo' => ['required','string'],
            'hf' => ['required','string'],
            'p1' => ['required','string'],
            'r1' => ['required','string'],
            't1' => ['required','string'],
            'p2' => ['required','string'],
            'r2' => ['required','string'],
        ];
    }
}
