<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    //
    protected $table = 'concepto';
    protected $fillable = [
        'clave',
        'descripcion',
        'unidad',
        'precio',
        'tipo',    
    ];
}
