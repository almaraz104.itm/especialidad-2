<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CultivoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'cultivo' => ['required','string'],
            'fsiembra' => ['required','string'],
            'fcosecha' => ['required','string'],
            'fsoca' => ['required','string'],
            'fuente_ab' => ['required','string'],
        ];
    }
}
