<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="recibo.css" type="text/css">
  <title>Recibo</title>
  <style>
    body{
      font-family: sans-serif;

    }
    .logo{
      width: 20%;
      text-align: left;
      margin-bottom: -30px;
      margin-left: 10px;
    }
    .folio{
      float: right;
      height: 30px;
    }
    h3{
      text-align: center;
      margin-top: -30px;
      
    }
    h5{
      text-align: center;
      margin-top: -25px;
    }
    .rfc{
      text-align: center;
      margin-top: -20px;
    }
    p{
      text-align: center;
    }
    table{
      margin: 0 auto;
      width: 100%;
      border: 1px solid #000000;
       border-collapse: collapse;
      text-align: center;
    }

    td{
      border: 1px solid #000000;
      padding: 2px;
    }
    tr{

    }
    .sb{
      border: none;
    }
    .sbd{
      border: none;
      text-align: : right;
    }
    .sbs{
      border: none;
      text-decoration: underline;
    }
   


  </style>
  <img class="logo" src={{asset('Images/logo.jpg')}} alt="Logo"/>
  <input type="text" name="" value="RECIBO N°: {{ $datosCobro->id }}" class="folio">
</head>

<body>

    <h3>ASOCIACIÓN DE AGRICULTORES DE ALVARO OBREGÓN TARIMBARO AC.</h3>
    <br/>
    <h5>LBIO. DE ALVARO OBREGON KM 49 900 SN SC ALVARO O. MICH ENTRE CARR. ACAMBARO-MORELIA</h5>
    <br/>
    <h5 class="rfc">RFC AAV931123DW0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REGIMEN DE PERSONAS MORALES CON FINES NO LUCRATIVOS</h5>


  <table class="tabla">
    <thead>
     
    </thead>
    <tbody class="tbody">
  
     <tr style="background-color: #F2F2F2">
        <td colspan="2">Tarjeta</td>
        <td colspan="4">Nombre</td>
        <td colspan="4">RFC</td>
      </tr>
      <tr>
        <td colspan="2">{{ $datosCobro->tarjeta }}</td>
        <td colspan="4">{{ $datosCobro->nombre }}</td>
        <td colspan="4">NA</td>
      </tr>
       <tr style="background-color: #F2F2F2">
        <td colspan="6">Domicilio</td>
        <td colspan="4">Lugar y fecha de expedición</td>
      </tr>
      <tr>
         <td colspan="6">NA</td>
        <td colspan="4">Alvaro Obregón Mich. {{ $datosCobro->fecha }}</td>
      </tr>
      <tr style="background-color: #F2F2F2">
        <td colspan="1">Cuenta</td>
        <td colspan="1">Sección</td>
        <td colspan="1">Ejido</td>
        <td colspan="1" >Municipio</td>
        <td colspan="1" >Concepto</td>
        <td colspan="1" >Ciclo Año</td>
        <td colspan="1" >Cuota</td>
        <td colspan="1" >Riego</td>
        <td colspan="1" >Sup</td>
        <td colspan="1" >Importe</td>
      </tr>

      <tr>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->cuenta }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->seccion }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->ejido }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->municipio }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->concepto }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->ciclo }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->cuota }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->riegos }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->superficie }}</td>
          <td colspan="1" class="sb"><br></br>{{ $datosCobro->total }}</td>
      </tr>
      <tr>
        <td colspan="10" class="sbs"><br></br><br></br><br></br><br></br><br></br><strong><h5>En el entendido que de no tener limpios sus canales regadores, no se le proporcionara el servicio de riego</h5></strong></td>
      </tr>
      <tr>
        <td class="sbd" colspan="10"><strong>MAQUINARIA: &nbsp;&nbsp;&nbsp;$ {{ $datosCobro->maquinaria }}</strong></td>
      </tr>
      <tr>
        <td class="sbd" colspan="10"><strong>DRENAJE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ {{ $datosCobro->drenaje }}</strong></td>
      </tr>
      <tr>
        <td class="sbd" colspan="10"><strong>RIEGO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ {{ $datosCobro->servicio }}</strong></td>
      </tr>
      <tr>
        <td class="sbd" colspan="10"><strong>TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ {{ $datosCobro->total }}</strong></td>
      </tr>


       <tr>
        <td colspan="10" class="sb"><br></br><br></br><br></br><br></br><br></br><strong>SOLICITAR SU SERVICIO DE RIEGO 8 DÍAS ANTES</strong></td>
      </tr>
     
    </tbody>
  </table>  
</body>
</html>