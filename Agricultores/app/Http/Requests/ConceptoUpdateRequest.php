<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConceptoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'descripcion' => ['required','string','min:1','max:50'],
            'unidad' => ['required','string','min:1','max:25'],
            'precio' => ['required','string','min:1','max:25'],
            'tipo' => ['required','string'],
        ];
    }
}
