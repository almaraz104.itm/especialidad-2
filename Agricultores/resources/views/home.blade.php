@extends('admin.layout')

@section('content')

 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->
<br/> 



<div class="container">


<div class="card shadow text-white" style="background-color:rgba(0, 120, 155,  0.6); margin:2px; text-align: center;"><h4>Cobros</h4></div>
 <div class="card shadow">
	<form method="POST" action="{{route('cobros.store')}}">
        @csrf
		<div class="form-group row" style="margin: 10px">

                            <div class="form-group col-md-1">
                       			<label for="factura" class="control-label">{{ __('Factura') }}</label>
		                          <input id="factura" type="text" class="form-control @error('factura') is-invalid @enderror" name="factura" value="{{ old('factura') }}" required autocomplete="factura" placeholder="N-" autofocus>
		                               @error('factura')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
                   			 </div>
              			 
    				

                   		    <div class="form-group col-md-4">
                       			<label for="nombre" class="control-label">{{ __('Nombre') }}</label>
		                          <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
		                               @error('nombre')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="tarjeta" class="control-label">{{ __('Tarjeta') }}</label>
		                          <input id="tarjeta" type="text" class="form-control @error('tarjeta') is-invalid @enderror" name="tarjeta" value="{{ old('tarjeta') }}" required autocomplete="tarjeta" autofocus>
		                               @error('tarjeta')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>



                   		    <div class="form-group col-md-1">
                       			<label for="no" class="control-label">{{ __('No') }}</label>
		                          <input id="no" type="text" class="form-control @error('no') is-invalid @enderror" name="no" value="0" required autocomplete="no" autofocus>
		                               @error('no')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
                          

                   			<div class="form-group col-md-2">
                       			<label for="fecha" class="control-label">{{ __('Fecha') }}</label>
		                          <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha" autofocus>
		                               @error('fecha')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
        </div>

        <div class="container bg-primary text-center">
        	<label>Cooperaciones</label>
        </div>
        

        <div class="form-group row" style="margin: 10px">

                            <div class="form-group col-md-1">
                       			<label for="clave" class="control-label">{{ __('Clave') }}</label>
		                          <input id="clave" type="text" class="form-control @error('clave') is-invalid @enderror" name="clave" value="0"  autocomplete="clave" autofocus>
		                               @error('clave')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
                   			 </div>

                   		    <div class="form-group col-md-5">
                       			<label for="acuerdo" class="control-label">{{ __('Acuerdo') }}</label>
		                          <input id="acuerdo" type="text" class="form-control @error('acuerdo') is-invalid @enderror" name="acuerdo" value="NA" autocomplete="acuerdo" autofocus>
		                               @error('acuerdo')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-1">
                       			<label for="tipo" class="control-label">{{ __('Tipo') }}</label>
		                          <input id="tipo" type="text" class="form-control @error('tipo') is-invalid @enderror" name="tipo" value="NA" autocomplete="tipo" autofocus>
		                               @error('tipo')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   		    <div class="form-group col-md-1">
                       			<label for="ano" class="control-label">{{ __('Año') }}</label>
		                          <input id="ano" type="text" class="form-control @error('ano') is-invalid @enderror" name="year" value="0"  autocomplete="ano" autofocus>
		                               @error('ano')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
                          
                            <div class="form-group col-md-2">
                       			<label for="caracter" class="control-label">{{ __('Carácter') }}</label>
		                          <input id="caracter" type="text" class="form-control @error('caracter') is-invalid @enderror" name="caracter" value="NA"  autocomplete="caracter" autofocus>
		                               @error('caracter')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="cuota" class="control-label">{{ __('Cuota') }}</label>
		                          <input id="cuota" type="cuota" class="form-control @error('cuota') is-invalid @enderror coo" name="cuota" value="0" required autocomplete="cuota" autofocus  onkeyup="pintar();">
		                               @error('cuota')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
        </div>

        <div class="container bg-info text-center">
        	<label>Datos de parcela</label>
        </div>


        <div class="form-group row" style="margin: 10px">

                            <div class="form-group col-md-2">
                       			<label for="cuenta" class="control-label">{{ __('Cuenta') }}</label>
		                          <input id="cuenta" type="text" class="form-control @error('cuenta') is-invalid @enderror" name="cuenta" value="0" required autocomplete="cuenta" autofocus>
		                               @error('cuenta')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
                   			 </div>

                   		    <div class="form-group col-md-2">
                       			<label for="seccion" class="control-label">{{ __('Sección') }}</label>
		                          <input id="seccion" type="text" class="form-control @error('seccion') is-invalid @enderror" name="seccion" value="0" required autocomplete="seccion">
		                               @error('seccion')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="ejido" class="control-label">{{ __('Ejido') }}</label>
		                          <select class="form-control" name="ejido" required>
                                    <option selected="">{{'-- Opción --'}}</option>
                                  
                                         @foreach($ejidos as $item)
                                             <option>{{$item->nombre}}</option>
                                         @endforeach
								
                                </select>
                   			</div>

                            <div class="form-group col-md-2">
                       			<label for="municipio" class="control-label">{{ __('Municipio') }}</label>
		                         <select class="form-control" name="municipio" required>
                                    <option selected="">{{'-- Opción --'}}</option>
                                             <option>Alvaro Obregón</option>
                                             <option>Tarimbaro</option>
                                    
								
                                </select>
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="superficie" class="control-label">{{ __('Superficie') }}</label>
		                          <input id="superficie" type="text" class="form-control @error('superficie') is-invalid @enderror" name="superficie" value="0" required autocomplete="superficie">
		                               @error('superficie')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
        </div>

          <div class="container bg-primary text-center">
        	<label>Concepto de pago por parcela</label>
        </div>

         <div class="form-group row" style="margin: 10px">

                            <div class="form-group col-md-2">
                       			<label for="concepto" class="control-label">{{ __('Concepto') }}</label>
		                        <select class="form-control" name="concepto" required>
                                    <option>{{'--- Opción ---'}}</option>
                                   
                                         @foreach($conceptos as $item)
                                             <option>{{$item->clave}}</option>
                                         @endforeach
									
                                </select>
                   			 </div>

                   		    <div class="form-group col-md-1">
                       			<label for="tipo2" class="control-label">{{ __('Tipo') }}</label>
		                          <select class="form-control" name="tipo2" required>
                                    <option>{{'-Op-'}}</option>
                                  
                                         @foreach($conceptos as $item)
                                             <option>{{$item->tipo}}</option>
                                         @endforeach
								
                                </select>
                   			</div>

                   			<div class="form-group col-md-1">
                       			<label for="ciclo" class="control-label">{{ __('Ciclo-Año') }}</label>
		                          <input id="ciclo" type="text" class="form-control @error('ciclo') is-invalid @enderror" name="ciclo" value="0" required autocomplete="ciclo" autofocus>
		                               @error('ciclo')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   		    <div class="form-group col-md-1">
                       			<label for="unidad" class="control-label">{{ __('Unidad') }}</label>
		                          <input id="unidad" type="text" class="form-control @error('unidad') is-invalid @enderror" name="unidad" value="HA" required autocomplete="unidad" autofocus>
		                               @error('unidad')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>
                          
                            <div class="form-group col-md-2">
                       			<label for="cuota2" class="control-label">{{ __('Cuota') }}</label>
		                          <input id="cuota2" type="text" class="form-control @error('cuota2') is-invalid @enderror" name="cuota2" value="0" required autocomplete="cuota2" autofocus onkeyup="multi()">
		                               @error('cuota2')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-1">
                       			<label for="riegos" class="control-label">{{ __('Riegos') }}</label>
		                          <input id="riegos" type="riegos" class="form-control @error('riegos') is-invalid @enderror" name="riegos" value="0" required autocomplete="riegos" autofocus>
		                               @error('riegos')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="cantidad" class="control-label">{{ __('Cantidad') }}</label>
		                          <input id="cantidad" type="cantidad" class="form-control @error('cantidad') is-invalid @enderror" name="cantidad" value="0" required autocomplete="cantidad" autofocus onkeyup="multi()">
		                               @error('cantidad')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

                   			<div class="form-group col-md-2">
                       			<label for="importe" class="control-label">{{ __('Importe') }}</label>
		                          <input id="importe" type="riegos" class="form-control @error('importe') is-invalid @enderror monto" name="importe" value="0" required autocomplete="importe" autofocus onkeyup="suma();" readonly >
		                               @error('importe')
		                                	<span class="invalid-feedback" role="alert">
		                                    		<strong>{{ $message }}</strong>
		                               		 </span>
		                                @enderror
                   			</div>

        </div>


         <div class="container bg-warning text-center">
        	<label style="color: #000">Cobrar</label>
        </div>

        <div class="form-group row" style="margin: 10px">

                            <div class="form-group col-md-2">
                       			<label for="cooperaciones" class="control-label">{{ __('COOPERACIONES') }}</label>
		                          <input id="cooperaciones" type="text" class="form-control @error('cooperaciones') is-invalid @enderror monto " name="cooperaciones" value="0" required autocomplete="cooperaciones"  onkeyup="suma();">        
                   			 </div>

                   			   <div class="form-group col-md-2">
                       			<label for="servicio" class="control-label">{{ __('SERVICIO DE RIEGO') }}</label>
		                          <input id="servicio" type="text" class="form-control @error('servicio') is-invalid @enderror monto" name="servicio" value="0" required autocomplete="servicio" onkeyup="suma();">        
                   			 </div>

                   			  <div class="form-group col-md-2">
                       			<label for="maquinaria" class="control-label">{{ __('MAQUINARIA') }}</label>
		                          <input id="maquinaria" type="text" class="form-control @error('maquinaria') is-invalid @enderror monto" name="maquinaria" value="0" required autocomplete="maquinaria" onkeyup="suma();">        
                   			 </div>

                   			 <div class="form-group col-md-2">
                       			<label for="drenaje" class="control-label">{{ __('S.DRENAJE') }}</label>
		                          <input id="drenaje" type="text" class="form-control @error('sdre') is-invalid @enderror monto" name="drenaje" value="0" required autocomplete="drenaje" onkeyup="suma();">        
                   			 </div>

                   			 <div class="form-group col-md-2">
                       			<label for="total" class="control-label">{{ __('TOTAL') }}</label>
		                          <input id="resultado" type="text" class="form-control @error('total') is-invalid @enderror bg-secondary" name="total"  readonly>

		                
                   			 </div>

                   			<div class="form-group col-md-2">
                       			<button type="submit" class="btn btn-outline-dark btn-lg btn-block" style="margin-top: 25px">Cobrar</button>
            
                   			 </div>    			

        </div>

        	<div class="row justify-content-center">
        		<a href="{{ route('recibo.pdf') }}" class="">
  				  <h1><i class="fas fa-print"></i></h1>
				</a>
        	</div>
        	  <span class="d-flex justify-content-center" style="font-size: 10px; margin-top: -10px">Imprimir recibo</span>  

        <br/>
     </form>


	</div>

	
	


</div>

<!-- Button trigger modal -->
<a href="#" class="btn btn-outline-dark back-to-top" data-toggle="modal" data-target="#exampleModalCenter">
<i class="fas fa-window-restore"></i>
</a>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Conceptos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <table class="table table-hover table-responsive-sm">

				  <thead>
				    <tr class="bg-info table-active">
				     
				      <th scope="col">Clave</th>
				      <th scope="col">Descripción</th>
				      <th scope="col">Precio</th>
				      <th scope="col">Tipo</th>
				      
				    </tr>
				  </thead>
				  <tbody>
				  	<!--
				     @foreach($conceptos as $item)
	                        <tr>
	                          
	                            <td>{{$item->clave}}</td>
	                              <td>{{$item->descripcion}}</td>
	                                  <td>{{$item->precio}}</td>
	                                    <td>{{$item->tipo}}</td>
	                                                   
	                        </tr>

    				 @endforeach
    				-->
  				</tbody>
		</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--OPERACIONES-->

<script>
function suma(){
		var total = 0;

		$(".monto").each(function(){
			if (isNaN(parseFloat($(this).val()))) {
				total += 0;
			} else {
			  total += parseFloat($(this).val());
	  }
	});

	document.getElementById('resultado').value = total;
}
</script>


<script>
function pintar(){
		var total = 0;

		$(".coo").each(function(){
			if (isNaN(parseFloat($(this).val()))) {
				total = 0;
			} else {
			  total = parseFloat($(this).val());
	  }
	});

	document.getElementById('cooperaciones').value = total;
}
</script>


<script >
 function multi()
 {
 var total = 0;
 var cuota2 = document.getElementById("cuota2").value;
 var cantidad = document.getElementById("cantidad").value;

 total = (cuota2 * cantidad);

 document.getElementById('importe').value = total;

 }
</script>







<!--
<input id="val1"  onkeydown="multi()" />
 <input id="val2" value="10" onkeydown="multi()" readonly />
 <input id="dis">
 <span id="Display"></span>

<script >
 function multi()
 {
 var total = 0;
 var valor1 = document.getElementById("val1").value;
 var valor2 = document.getElementById("val2").value;

 total = (valor1 * valor2);

 document.getElementById('dis').value = total;

 }
</script>

-->
@endsection
