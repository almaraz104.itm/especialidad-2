@extends('admin.layout')

@section('content')

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow p-1">

			@foreach($usuarios as $item)
                <div class="card-header h5 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6);">
                    Usuario: {{$item->username}}

                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                        <th scope="row">Nombre:</th>
                        <td>{{$item->nombre}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido paterno:</th>
                        <td>{{$item->apaterno}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido materno:</th>
                        <td>{{$item->amaterno}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Teléfono:</th>
                        <td>{{$item->telefono}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Correo electrónico:</th>
                        <td>{{$item->email}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Teléfono:</th>
                        <td>{{$item->telefono}}</td>
                        </tr>
                        
                        
                    </table>

                      <div class="text-right">
                          <a href="{{ route('usuarios.index')}}" class="btn btn-sm btn-link">
                            <h1> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
             
            </div>
            @endforeach
        </div> 

    </div>



    
</div>



@endsection