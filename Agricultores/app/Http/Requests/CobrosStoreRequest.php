<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CobrosStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'factura' => ['required','numeric'],
            'nombre' => ['required','string'],
            'tarjeta' => ['required','string'],
            'no' => ['required','numeric'],
            'superficie' => ['required','numeric'],
            'fecha' => ['required'],
            'clave' => ['required','string'],
            'acuerdo' => ['required','string'],
            'tipo' => ['required','string'],
            'year' => ['required','numeric'],
            'caracter' => ['required','string'],
            'cuota' => ['required','numeric'],
            'cuenta' => ['required','numeric'],
            'seccion' => ['required','numeric'],
            'ejido' => ['required','string'],
            'municipio' => ['required','string'],
            'concepto' => ['required','string'],
            'tipo2' => ['required','string'],
            'ciclo' => ['required','string'],
            'unidad' => ['required','string'],
            'cuota2' => ['required','numeric'],
            'riegos' => ['required','numeric'],
            'cantidad' => ['required','numeric'],
            'importe' => ['required','numeric'],
            'cooperaciones' => ['required','numeric'],
            'servicio' => ['required','numeric'],
            'maquinaria' => ['required','numeric'],
            'drenaje' => ['required','numeric'],
            'total' => ['required','numeric'],
            
        ];
    }
}
