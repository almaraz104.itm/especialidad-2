<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adeudos extends Model
{
    //
    protected $table = 'adeudos';
    protected $fillable = [
        'factura',
        'tarjeta',
        'usuario',
        'cuenta',
        'seccion',
        'ejido',
        'municipio',
        'cultivo',
        'superficie',
        'concepto',
        'tipo',
        'ciclo',
        'unidad',
        'cuota2',
        'riegos',
        'cantidad',
        'importe',
        'fecha',
         
    ];

}
