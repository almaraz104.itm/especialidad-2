<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuariosStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username' => ['required','max:25','unique:users'],
            'nombre' => ['required','alpha','min:3','max:25'],
            'apaterno' => ['required','alpha','min:1','max:25'],
            'amaterno' => ['required','alpha','min:1','max:25'],
            'telefono' => ['required','numeric', 'digits_between:10,13'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
