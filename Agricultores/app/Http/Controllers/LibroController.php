<?php

namespace App\Http\Controllers;

use App\Http\Requests\LibroStoreRequest;
use Illuminate\Http\Request;
use App\Concepto;
use App\Cultivo;
use App\Adeudos;
use App\Libro;
use DB;


class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $libros=Libro::all();
        return view('canaleros.libro.index',compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LibroStoreRequest $request)
    {
        //
        $datosLibro=request()->except('_token');
        Libro::insert($datosLibro);

        return redirect('/libros')->with('Mensaje','¡Libro guardado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $libro = DB::table('libro')
                    ->where('id', '=', $id)
                    ->get();
        return view('canaleros.libro.ver',compact('libro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libro $libro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Libro::destroy($id);
        return redirect('/libros')->with('Mensaje','¡Libro eliminado exitosamente!');
    }

    public function agregar(){
        $conceptos = Concepto::all();
        $cultivos=Cultivo::all();
        return view('canaleros.libro.nuevo',compact('cultivos','conceptos'));
    }

    public function adeudos(Request $request){

        $datosLibro=request()->except('_token');
        Adeudos::insert($datosLibro);

        return redirect('/libros')->with('Mensaje','¡Adeudo registrado exitosamente!');
    }

}
