<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PadronUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre' => ['required','string'],
            'tarjeta' => ['required','string'],
            'cuenta' => ['required','numeric'],
            'municipio' => ['required','string'],
            'ejido' => ['required','string'],
            'seccion' => ['required','numeric'],
            'pro' => ['required','numeric'],
            'sup_fis' => ['required','numeric'],
            'ejido_no' => ['required','numeric'],
            'sistema' => ['required','alpha'],
            'canalero' => ['required','string'],
            's_cta' => ['required','numeric'],
            'doc' => ['required','alpha'],
            'no' => ['required','string'],
            'fecha' => ['required'],
            'tarjeta_ant' => ['required','string'],
            'usipad' => ['required','string'],
            'cta_sipad' => ['required','numeric'],
            'sup_fisica' => ['required'],
            'sup_riego' => ['required'],
            'sec_orig' => ['required','numeric'],
        ];
    }
}
