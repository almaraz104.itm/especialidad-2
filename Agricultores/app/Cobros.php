<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cobros extends Model
{
    //
    protected $table = 'cobros';
    protected $fillable = [
        'factura',
        'nombre',
        'tarjeta',
        'no',
        'superficie',    
        'fecha',
        'clave', 
        'acuerdo',
        'tipo',
        'year',   
        'caracter', 
        'cuota', 
        'cuenta', 
        'seccion', 
        'ejido', 
        'municipio',
        'concepto',
        'tipo2',
        'ciclo',
        'unidad',
        'cuota2',
        'riegos', 
        'cantidad', 
        'importe',  
        'cooperaciones', 
        'servicio',
        'maquinaria', 
        'drenaje', 
		'total', 

    ];
}
