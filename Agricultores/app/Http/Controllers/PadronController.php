<?php

namespace App\Http\Controllers;

use App\Http\Requests\PadronStoreRequest;
use App\Http\Requests\PadronUpdateRequest;
use Illuminate\Http\Request;
use App\Canaleros;
use App\Padron;
use DB;


class PadronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $padron = Padron::buscarpor($tipo, $buscar)->paginate(50)->appends($variablesUrl);
        return view('padron.index',compact('padron'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PadronStoreRequest $request)
    {
 
    
        $datosPadron=request()->except('_token');
        Padron::insert($datosPadron);
        return redirect('/padron=lista')->with('Mensaje','¡Registro guardado exitosamente!');
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Padron  $padron
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $padron = DB::table('padron')
                    ->where('id', '=', $id)
                    ->get();
        return view('padron.ver',compact('padron'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Padron  $padron
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        /*
       $padron=Padron::findOrFail($id);
       return view('padron.editar',compact('padron'));*/
       $canaleros =Canaleros::all();
       $padron = DB::table('padron')
                    ->where('id', '=', $id)
                    ->get();
        return view('padron.editar',compact('padron','canaleros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Padron  $padron
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $datosPadron=request()->except(['_token','_method']);
        Padron::where('id','=',$id)->update($datosPadron);

        return redirect('/padron=lista')->with('Mensaje','¡Ejidatario modificado exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Padron  $padron
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       Padron::destroy($id);
       return redirect('/padron=lista')->with('Mensaje','¡Registro eliminado exitosamente!');
    }

    public function todo(Request $request){

        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $padron = Padron::buscarpor($tipo,$buscar)->paginate(20)->appends($variablesUrl);
        return view('padron.padron',compact('padron'));

    }

    public function agregar(){
        $canaleros = Canaleros::all();
        return view ('padron.nuevo', compact('canaleros'));
    }

}
