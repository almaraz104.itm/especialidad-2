<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canaleros extends Model
{
    //
    protected $table = 'canalero';
    protected $fillable = [
        'nombre',
        'seccion',
        'direccion',
        'telefono',
         
    ];
}
