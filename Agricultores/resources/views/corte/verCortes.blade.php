@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))

    <div class="alert alert-success " role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                   Cortes realizados por día
                </div>

                <div class="card-body">

<table class="table table-hover table-responsive-lg text-center">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="colspan=3">Total</th>
      <th scope="colspan=3">Fecha del corte</th>
      
    </tr>
  </thead>
  <tbody>
     @foreach($datosCorte as $item)
                        <tr>
                          
                            <td>$ {{$item->total}}</td>
                              <td>{{$item->created_at}}</td>
                                               
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>
   

                </div>
            </div>
        </div>
    </div>

        <div class="text-right">
                          <a href="{{ route('corte.index')}}" class="btn btn-sm btn-link">
                            <h1 class="display-4"> <i class="fas fa-long-arrow-alt-left"></i></h1>
                          </a>    
                      </div>
</div>


@endsection