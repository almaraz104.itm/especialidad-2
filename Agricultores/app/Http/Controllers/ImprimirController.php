<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImprimirStoreRequest;
use Illuminate\Http\Request;
use App\Imprimir;
use DB;


class ImprimirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('canaleros.hoja.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImprimirStoreRequest $request)
    {
        //

        $datosLibro=request()->except('_token');
        Imprimir::insert($datosLibro);

        return redirect('/hoja')->with('Mensaje','Da click en el boton ver para imprimir la hoja');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Imprimir  $imprimir
     * @return \Illuminate\Http\Response
     */
    public function show(Imprimir $imprimir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Imprimir  $imprimir
     * @return \Illuminate\Http\Response
     */
    public function edit(Imprimir $imprimir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Imprimir  $imprimir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Imprimir  $imprimir
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       DB::table('hoja')->truncate();
       return redirect('/hoja')->with('Mensaje','¡Datos de la hoja eliminados exitosamente!');
    }

      public function ver()
    {
        //
        $libro = Imprimir::all();
        return view('canaleros.hoja.ver',compact('libro'));
    }
}
