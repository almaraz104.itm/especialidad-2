<?php

namespace App\Http\Controllers;
use Barryvdh\DomPDF\Facade as PDF;
use App\Ingresos;
use DB;
use Illuminate\Http\Request;

class IngresosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $variablesUrl= $request->all();
        $Ingresos = Ingresos::buscarpor($tipo, $buscar)->paginate(50)->appends($variablesUrl);
        return view('ingresos.index',compact('Ingresos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ingresos  $ingresos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $Ingresos = DB::table('ingresos')
                    ->where('id', '=', $id)
                    ->get();
        return view('ingresos.ver',compact('Ingresos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ingresos  $ingresos
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingresos $ingresos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ingresos  $ingresos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingresos $ingresos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ingresos  $ingresos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Ingresos::destroy($id);
         return redirect('/ingresos')->with('Mensaje','¡Registro eliminado exitosamente!');
    }

    public function pdf()
    {


       $datosIngresos = Ingresos::all();      
                   
        $pdf = PDF::loadView('ingresos.pdf', compact('datosIngresos'));

         return $pdf->download('Ingresos.pdf');

      
    }

}
