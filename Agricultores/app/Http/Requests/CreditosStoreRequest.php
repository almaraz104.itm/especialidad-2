<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreditosStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'nombre' => ['required','string'],
            'tarjeta' => ['required','string'],
            'telefono' => ['required','numeric', 'digits_between:10,13'],
            'calle' => ['required','string'],
            'noexterior' => ['required','string'],
            'nointerior' => ['required','string'],
            'cpostal' => ['required','string','min:5','max:5'],
            'municipio' => ['required','string'],
            'estado' => ['required','string'],

        ];
    }
}

