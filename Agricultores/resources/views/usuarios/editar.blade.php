@extends('admin.layout')

@section('content')

<br/>

@foreach($usuarios as $item)
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow p-1">
                
                <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Editar usuario'.' '.': '. $item->username) }}</strong></h4></div>

                <div class="card-body">
                    <form method="POST" action="{{route('usuarios.update',$item->id)}}">
                        @csrf
                        {{method_field('PATCH')}}

                    <div class="form-group">
                        <label for="username" class="control-label">{{ __('Username') }}</label>
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $item->username}}" required autocomplete="username" autofocus style="text-transform:capitalize;">
                                     @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    <div class="form-group">
                        <label for="nombre" class="control-label">{{ __('Nombre') }}</label>
                            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ $item->nombre}}" required autocomplete="nombre" autofocus style="text-transform:capitalize;">
                                     @error('nombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    <div class="form-group">
                        <label for="apaterno" class="control-label">{{ __('Apellido Paterno') }}</label>
                            <input id="apaterno" type="text" class="form-control @error('apaterno') is-invalid @enderror" name="apaterno" value="{{ $item->apaterno}}" required autocomplete="apaterno" autofocus style="text-transform:capitalize;">
                                     @error('apaterno')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    <div class="form-group">
                        <label for="amaterno" class="control-label">{{ __('Apellido Materno') }}</label>
                            <input id="amaterno" type="text" class="form-control @error('amaterno') is-invalid @enderror" name="amaterno" value="{{ $item->amaterno}}" required autocomplete="amaterno" autofocus style="text-transform:capitalize;">
                                     @error('amaterno')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    <div class="form-group">
                        <label for="telefono" class="control-label">{{ __('Teléfono') }}</label>
                            <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ $item->telefono}}" required autocomplete="telefono" autofocus>
                                     @error('telefono')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label">{{ __('Correo Electrónico') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $item->email}}" required autocomplete="email" autofocus>
                                     @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>

                    
                    <div class="form-group">
                        <label for="password" class="control-label">{{ __('Contraseña') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="" placeholder="********" required autocomplete="password" autofocus>
                                     @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                    </div>


                
                        <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('usuarios.index') }}" class="btn btn-link">
                                   
                                        <h1> <i class="fas fa-times"></i></h1>  
                                    
                                 </a>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                     <h1> <i class="far fa-save"></i></h1>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>             
            </div>
        </div>
    </div>
</div>

@endforeach

@endsection















