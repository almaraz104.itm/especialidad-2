<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style>
		body{
			font-family: sans-serif;
		}
		.logo{
			width: 25%;
			text-align: left;
			margin-bottom: -30px;
		}
		
		.tabla{
			width: 100%;
			border: 2px solid #999999;
			border-collapse: collapse;
			
		}

		.tbody{
			text-align: center;
			border-collapse: collapse;
   			padding: 0.3em;

		}
		th {
		   background: #eee;
		}
		h3{
			text-align: center;
			margin-top: -50px;
			font-weight: bold;

		}
		.total{
			background-color: #eee;
			float: right;
			border: 1px solid #999999;
			padding: 0.4em;
			font-weight: bold;

		}
	</style>

	 <img class="logo" src={{asset('Images/logo.jpg')}} alt="Logo"/>
	

</head>
<body>
 <h3>Reporte del día {{ $fechaActual }}</h3>
	<table class="tabla">
		<thead>
			<tr>
				<th>Cooperaciones</th>
				<th>Servicios de Riego</th>
				<th>Maquinaria</th>
				<th>Sistema de drenaje</th>
				<th>Importe</th>	
			</tr>
		</thead>
		<tbody class="tbody">
			@foreach($datosCorte as $item)
			<tr>
				<td>{{ $item->cooperaciones }}</td>
				<td>{{ $item->servicio }}</td>
				<td>{{ $item->maquinaria }}</td>
				<td>{{ $item->drenaje }}</td>
				<td>{{ $item->importe }}</td>
			</tr>
			@endforeach


		</tbody>
	</table>

	<div class="total">
		TOTAL:
	 $ {{ $sumaImportes }}
	</div>
</body>
</html>