<?php

namespace App\Http\Controllers;

use App\Http\Requests\CultivoStoreRequest;
use App\Http\Requests\CultivoUpdateRequest;
use Illuminate\Http\Request;
use App\Cultivo;
use DB;


class CultivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cultivos = Cultivo::paginate(15);
        return view('cultivo.index',compact('cultivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CultivoStoreRequest $request)
    {
        //
        
        $datosCultivo=request()->except('_token');
        Cultivo::insert($datosCultivo);
 
        return redirect('/cultivo')->with('Mensaje','¡Cultivo guardado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cultivo  $cultivo
     * @return \Illuminate\Http\Response
     */
    public function show(Cultivo $cultivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cultivo  $cultivo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $cultivos = DB::table('cultivo')
                    ->where('id', '=', $id)
                    ->get();
        return view('cultivo.editar',compact('cultivos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cultivo  $cultivo
     * @return \Illuminate\Http\Response
     */
    public function update(CultivoUpdateRequest $request, $id)
    {
        //
        
        $datosCultivo=request()->except(['_token','_method']);
         Cultivo::where('id','=',$id)->update($datosCultivo);

        return redirect('/cultivo')->with('Mensaje','¡Cultivo modificado exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cultivo  $cultivo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Cultivo::destroy($id);
        return redirect('/cultivo')->with('Mensaje','¡Cultivo eliminado exitosamente!');
    }

    public function agregar()
    {
        //
        return view('cultivo.nuevo');
    }
}
