<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Padron extends Model
{
    //
    protected $table = 'padron';
    protected $fillable = [
        'nombre',
        'tarjeta',
        'cuenta',
        'municipio',
        'ejido',
        'seccion',
        'cp',
        'lt',
        'slt',
        'ra',
        'pc',
        'te',
        'sr',
        'eq',
        'mp',
        'pro',
        'sup_fis',
        'ejido_no',
        'sistema',
        'canalero',
        's_cta',
        'doc',
        'no',
        'fecha',
        'tarjeta_ant',
        'usipad',
        'cta_sipad',
        'uni',
        'zo',
        'modu',
        'sra',
        'ssra',
        'est',
        'gr',
        'pre',
        'sup_fisica',
        'sup_riego',
        'sec_orig',
    ];


    public function scopeBuscarpor($query, $tipo, $buscar){
        if(($tipo) && ($buscar)){
            return $query->where($tipo,'like',"%$buscar%");
        }
    }
}
