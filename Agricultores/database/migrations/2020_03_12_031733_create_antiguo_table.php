<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAntiguoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antiguo', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->float('s_cta');
            $table->string('doc');
            $table->string('no');
            $table->date('fecha');
            $table->string('usipad');
            $table->float('cta_sipa');
            $table->integer('uni');
            $table->integer('zo');
            $table->integer('modu');
            $table->integer('sra');
            $table->integer('ssra');
            $table->integer('est');
            $table->integer('gr');
            $table->integer('pre');
            $table->float('sup_fisica');
            $table->float('sup_riego');
            $table->integer('sec_org');
            $table->string('tarj_ant');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antiguo');
    }
}
