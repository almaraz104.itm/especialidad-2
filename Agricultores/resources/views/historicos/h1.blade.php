@extends('admin.layout')

@section('content')

<br/> 

<!-- Filtros -->
<nav class="navbar navbar-light " style="background-color:rgba(0, 120, 155,  0.6);">
  <h2 class="text-white"><strong>Historico H1</strong></h2>

  <form class="form-inline">

      <select name="tipo" class="form-control mr-sm-2"  required>
        <option selected>Buscar por...</option>
        <option>FECHA</option>
        <option>USUARIO</option>
        <option>NOMBRE_DE_EJIDO</option>
      </select>

    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Buscar" required>
    <button class="btn btn-link" type="submit">
       <h2 style="margin-top: 10px; margin-left: -10px"><i class="fas fa-search text-white"></i></h2> 
    </button>

    <a href="{{ route('h1.index') }}" style="margin-top: 10px; margin-left: 5px"><h2><i class="fas fa-sync-alt text-white"></i></h2></a>

  </form>


</nav>
<!-- Fin Filtros -->

<table class="table table-bordered table-responsive">
  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Factura</th>
      <th scope="col">Fecha</th>
      <th scope="col">Tarjeta</th>
      <th scope="col">Usuario</th>
      <th scope="col">Ciclo Actual</th>
      <th scope="col">Cuenta</th>
      <th scope="col">Sec</th>
      <th scope="col">Eji</th>
      <th scope="col">Nombre Ejido</th>
      <th scope="col">Mpio</th>
      <th scope="col">Cultivo</th>
      <th scope="col">Superficie Fisica Total</th>
      <th scope="col">Concepto</th>
      <th scope="col">Tipo</th>
      <th scope="col">Ciclo Año</th>
      <th scope="col">Unidad</th>
      <th scope="col">Cuota por Unidad</th>
      <th scope="col">Riegos</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Importe</th>
    </tr>
  </thead>
  <tbody class="table-warning">
  	 @foreach($h1 as $item)
                        <tr>
                       		
                            <td>{{$item->FACTURA}}</td>
                             <td>{{$item->FECHA}}</td>
                              <td>{{$item->TARJETA}}</td>
                               <td>{{$item->USUARIO}}</td>
                               	<td>{{$item->CICLOACTUAL}}</td>
                               <td>{{$item->CUENTA}}</td>
                              <td>{{$item->SEC}}</td>
                             <td>{{$item->EJI}}</td>
                            <td>{{$item->NOMBRE_DE_EJIDO}}</td>
                                 <td>{{$item->MPIO}}</td>
                                    <td>{{$item->CULTIVO}}</td>
                                      <td>{{$item->SUP_FISICA_TOTAL}}</td>
                                         <td>{{$item->CONCEPTO}}</td>
                                           <td>{{$item->TIPO}}</td>
                                         <td>{{$item->CICLO_AÑO}}</td>
                                       <td>{{$item->UNIDAD}}</td>
                                     <td>{{$item->CUOTA_POR_UNIDAD}}</td>
                                   <td>{{$item->RIEGOS}}</td>
                                   <td>{{$item->CANTIDAD}}</td>
                                 <td>{{$item->IMPORTE}}</td>


                        </tr>
                        <tr>
                        	
                        </tr>
                 

    @endforeach
  </tbody>
</table>
{{--Paginación--}}
      <div class="row justify-content-center responsive">                       
           {{$h1->onEachSide(2)->links()}}                  
      </div>

{{-- Boton arriba flotante --}}
  <a href="#" class="btn btn-info back-to-top">
              <i class="fas fa-chevron-up"></i>
  </a>


@endsection