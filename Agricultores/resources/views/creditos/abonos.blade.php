@extends('admin.layout')

@section('content')


<!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->


<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">

                
                
                 
<!-- Filtros -->
<nav class="navbar navbar-light float-right" style="background-color:rgba(0, 120, 155,  0.6);">
  <h2 class="text-white"><strong>Abonos</strong></h2>

  <form class="form-inline" >

      
    
   

  </form>

</nav>
<!-- Fin Filtros -->

<div class="card-body">
 <table class="table table-hover table-responsive-lg ">

  <thead>
    <tr class="bg-info table-active">
     
      <th scope="col">Nombre</th>
      <th scope="col">Deuda</th>
      <th scope="col">Ahorro</th>

      <th></th>
      
      
    </tr>
  </thead>
  <tbody>
     @foreach($credito as $item)
                        <tr>
                          
                            <td>{{$item->nombre}}</td>
                              <td>{{$item->deuda}}</td>
                              <td>{{$item->ahorro}}</td>
                                

                                      <td width="10px">
                            
                                          <a href="{{route('creditos.edit',$item->id)}}"class="btn btn-sm btn-default" >
                                          
                                            ABONAR
                                          </a>
                       
                                      </td>
                                      
                                       
                        </tr>
                       
                 

     @endforeach
  </tbody>
</table>

 
                 

                    {{--Paginación--}}

                     <div class="row justify-content-center responsive">                       
                        {{$credito->onEachSide(2)->links()}}                  
                     </div>

                </div>
            </div>
        </div>
    </div>
</div>

  {{-- Boton arriba flotante --}}
  <a href="#" class="btn btn-info back-to-top">
              <i class="fas fa-chevron-up"></i>
  </a>


@endsection