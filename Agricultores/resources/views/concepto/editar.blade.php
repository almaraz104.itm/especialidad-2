@extends('admin.layout')

@section('content')



<br/>

@foreach($conceptos as $item)
 <div class="row justify-content-center">
        <div class="col-md-10 shadow p-2">
            <div class="card">
               <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>Editar Concepto: {{$item->clave}}</strong></h4></div>

                <div class="card-body">
                    <form method="POST" action="{{route('concepto.update',$item->id)}}">
                        @csrf
                        {{method_field('PATCH')}}

                        <div class="form-group row">
                            <label for="clave" class="col-md-4 col-form-label text-md-right">{{ __('Clave') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('clave')?'is-invalid':''}} " name="clave" id="clave" value="{{$item->clave}}" disabled>  
                                <div class="invalid-feedback"> 
                                    Clave ya registrada
                                </div>                         
                           </div>
                       </div>

                          <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('descripcion')?'is-invalid':''}} " name="descripcion" id="descripcion" value="{{$item->descripcion}}">  
                                <div class="invalid-feedback"> 
                                    El campo Descripción es obligatorio
                                </div>                         
                           </div>
                       </div>

                        <div class="form-group row">
                            <label for="unidad" class="col-md-4 col-form-label text-md-right">{{ __('Unidad') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('unidad')?'is-invalid':''}}" name="unidad" id="unidad" value="{{$item->unidad}}">
                                <div class="invalid-feedback"> 
                                    El campo Unidad es obligatorio
                                </div>             
                            </div>
                        </div>
 
                        <div class="form-group row">
                            <label for="precio" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control {{$errors->has('precio')?'is-invalid':''}}" name="precio" id="precio" value="{{$item->precio}}"> 
                                <div class="invalid-feedback"> 
                                    El campo Precio debe ser numerico
                                </div>            
                            </div>
                        </div>

            
                        <div class="form-group row">
                            <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="tipo" required>
                                    <option>{{$item->tipo}}</option>
                                   
                                         @foreach($cultivos as $item)
                                             <option>{{$item->clave}}</option>
                                         @endforeach

                                </select> 
                            </div>
                        </div>

                         <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('concepto.index') }}" class="btn btn-link">
                                   
                                    <h1> <i class="fas fa-times"></i></h1>  
                                    
                                 </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                     <h1> <i class="far fa-save"></i></h1>
                                </button>
                            </div>
                        </div>
    
                         </div>
                    </form>
                </div>
            </div>
        </div>

@endforeach
@endsection







