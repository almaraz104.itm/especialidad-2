@extends('admin.layout')

@section('content')

<br/>

@foreach($Adeudo as $item)
 <div class="row justify-content-center">
        <div class="col-md-8 shadow p-2">
            <div class="card">
               <div class="card-header bg-danger"><h4><strong>Abonar a la cuenta: {{$item->tarjeta}}</strong></h4></div>

                <div class="card-body">
                    <form method="POST" action="{{route('creditos.update',$item->id)}}">
                        @csrf
                        {{method_field('PATCH')}}

                        <div class="form-group row">
                            <label for="clave" class="col-md-3 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="usuario" id="usuario" value="{{$item->nombre}}" readonly>                          
                           </div>
                       </div>

                          <div class="form-group row">
                            <label for="descripcion" class="col-md-3 col-form-label text-md-right">{{ __('Ahorro') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="ahorro" id="" value="{{$item->ahorro}}" readonly>                          
                           </div>
                       </div>

                        <div class="form-group row">
                            <label for="unidad" class="col-md-3 col-form-label text-md-right">{{ __('Deuda') }}</label>
                            <div class="col-md-6">

                                <div class="input-group input-focus ">
                                  <div class="input-group-prepend ">
                                    <span class="input-group-text bg-white"><i class="fas fa-dollar-sign"></i></span>
                                  </div>
                                  <input type="search" placeholder="Search" class="form-control border-left-0" value="{{$item->deuda}}" name="deuda" id="num1" readonly>
                                </div>             
                            </div>
                        </div>
 
                        <div class="form-group row">
                            <label for="precio" class="col-md-3 col-form-label text-md-right">{{ __('Abono') }}</label>

                            <div class="col-md-6">

                                <div class="input-group input-focus ">
                                  <div class="input-group-prepend ">
                                    <span class="input-group-text bg-white"><i class="fas fa-dollar-sign"></i></span>
                                  </div>
                                  <input type="text" class="form-control border-left-0" name="abono" id="num2" value="0">
                                </div> 

                                <div class="invalid-feedback"> 
                                    El campo Abono debe ser numerico
                                </div>            
                            </div>
                        </div>
            <div class="d-flex justify-content-center">
                <input type="button" class="btn btn-sm btn-dark" value="Total $" onclick="operaciones('restar'); return false;" />&nbsp;&nbsp;
                <h4 id="resultado"></h4>
            </div>
                    <br/>

                         <div class="row">
                            <div class="col text-center">
                                <a href="{{ route('creditos.abonos') }}" class="btn btn-link">
                                   
                                     <h1> <i class="fas fa-times"></i></h1>  
                                    
                                 </a>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                    <h1> <i class="far fa-save"></i></h1>
                                </button>
                
                            </div>

                        </div>
    
                         </div>
                        
                    </form>
                </div>
            </div>
        </div>

@endforeach




<!-- Abonos -->
<script type="text/javascript">
    function operaciones(op)
{

    var ops = {
       

        restar: function restarNumeros(n1, n2) {
            return (parseInt(n1) - parseInt(n2));
        },
        
    };



    var num1 = document.getElementById("num1").value;
    var num2 = document.getElementById("num2").value;

    
    //Comprobamos si se ha introducido números en las cajas
    if (isNaN(parseFloat(document.getElementById('num1').value))) {
        document.getElementById('resultado').innerHTML="<span style='color: red;'>Campo adeudo no puede estar vacio</span>";
        document.getElementById("num1").innerText = "0";
        document.getElementById("num1").focus();
        } else if (isNaN(parseFloat(document.getElementById('num2').value))) {
        document.getElementById('resultado').innerHTML="<span style='color: red;'>Campo abono no puede estar vacio</span>";
        document.getElementById("num2").innerText = "0";
        document.getElementById("num2").focus();
    }
    else {
    //Si se han introducido los números en ámbas cajas, operamos:
        switch(op) {
            
            case 'restar':
                var resultado = ops.restar(num1, num2);
                document.getElementById('resultado').innerHTML="<span style='color: green;'>"+resultado+"</span>";
                break;
            
        }
    }

}
</script>




@endsection