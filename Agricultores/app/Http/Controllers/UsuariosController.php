<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsuariosStoreRequest;
use App\Http\Requests\UsuariosUpdateRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use DB;


class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuarios = User::paginate(5);
        return view('usuarios.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuariosStoreRequest $request)
    {
        
       
        //Recibo todos los datos con request y los mando al modelo para insertarlos en la tabla usuarios
        $datosUsuario=request()->all();
        User::insert([
            'username' => $datosUsuario['username'],
            'nombre' => $datosUsuario['nombre'],
            'apaterno' => $datosUsuario['apaterno'],
            'amaterno' => $datosUsuario['amaterno'],
            'telefono' => $datosUsuario['telefono'],
            'email' => $datosUsuario['email'],
            'password' => Hash::make($datosUsuario['password']),
        ]);

        //Redirecciono a la vista create con el mensaje de exito
        return redirect('/usuarios')->with('Mensaje','¡Usuario registrado exitosamente!');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $usuarios = DB::table('users')
                    ->where('id', '=', $id)
                    ->get();
        return view('usuarios.ver',compact('usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuarios = DB::table('users')
                    ->where('id', '=', $id)
                    ->get();
        return view('usuarios.editar',compact('usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(UsuariosUpdateRequest $request, $id)
    {
        
       
         $datosUsuario=request()->except(['_token','_method']);

         DB::table('users')
                        ->where('id', '=', $id)
                        ->update([
                            'username' => $datosUsuario['username'],
                            'nombre' => $datosUsuario['nombre'],
                            'apaterno' => $datosUsuario['apaterno'],
                            'amaterno' => $datosUsuario['amaterno'],
                            'telefono' => $datosUsuario['telefono'],
                            'email' => $datosUsuario['email'],
                            'password' => Hash::make($datosUsuario['password']),
                            ]);
         

        //Redirecciono a la vista listarusuarios con el mensaje de exito
         return redirect('/usuarios')->with('Mensaje','¡Datos de usuario modificados exitosamente!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        DB::table('users')
        ->where('id', '=', $id)->delete();*/       
        User::destroy($id);
        return redirect('/usuarios')->with('Mensaje','¡Usuario eliminado exitosamente!');
    }

     public function registry()
    {
        //
        return view('usuarios.nuevo');
    }
    
}
