<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcela', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->bigInteger('id_ejido')->unsigned();
            $table->foreign('id_ejido')->references('id')->on('ejidos');
            $table->bigInteger('id_municipio')->unsigned();
            $table->foreign('id_municipio')->references('id')->on('municipio');
            $table->bigInteger('id_canalero')->unsigned();
            $table->foreign('id_canalero')->references('id')->on('canalero');
            $table->date('fecha');
            $table->bigInteger('id_seccion')->unsigned();
            $table->foreign('id_seccion')->references('id')->on('seccion');
            $table->integer('cp');
            $table->integer('lt');
            $table->integer('slt');
            $table->integer('ra');
            $table->integer('pc');
            $table->bigInteger('id_tenencia')->unsigned();
            $table->foreign('id_tenencia')->references('id')->on('tenencia');
            $table->integer('sr');
            $table->boolean('eq');
            $table->float('sup_fisica');
            $table->integer('profundidad');
            $table->string('sistema');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcela');
    }
}