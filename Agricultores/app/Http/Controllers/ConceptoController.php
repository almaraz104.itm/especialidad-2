<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConceptoStoreRequest;
use App\Http\Requests\ConceptoUpdateRequest;
use Illuminate\Http\Request;
use App\Concepto;
use App\Cultivo;
use DB;

class ConceptoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $conceptos = Concepto::paginate(15);
        return view('concepto.index',compact('conceptos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConceptoStoreRequest$request)
    {
        
        
        $datosConcepto=request()->except('_token');
        Concepto::insert($datosConcepto);

        
        return redirect('/concepto')->with('Mensaje','¡Concepto guardado exitosamente!');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Concepto  $concepto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Concepto  $concepto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cultivos = Cultivo::all();
        $conceptos = DB::table('concepto')
                    ->where('id', '=', $id)
                    ->get();
        return view('concepto.editar',compact('conceptos','cultivos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Concepto  $concepto
     * @return \Illuminate\Http\Response
     */
    public function update(ConceptoUpdateRequest $request, $id)
    {
        //
       
        $datosConcepto=request()->except(['_token','_method']);
         Concepto::where('id','=',$id)->update($datosConcepto);

        return redirect('/concepto')->with('Mensaje','¡Concepto modificado exitosamente!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Concepto  $concepto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       Concepto::destroy($id);
        return redirect('/concepto')->with('Mensaje','¡Concepto eliminado exitosamente!');
    }

     public function agregar()
    {
        //
        
        return view('concepto.nuevo');
    }
}
