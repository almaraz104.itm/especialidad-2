<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cultivo extends Model
{
    //
    protected $table = 'cultivo';
    protected $fillable = [
        'clave',
        'cultivo',
        'fsiembra',
        'fcosecha',
        'fsoca',    
        'fuente_ab',
    ];
}
