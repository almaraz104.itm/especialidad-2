@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->



<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow ">
                
                <div class="card-header h2 font-weight-bold text-white" style="background-color:rgba(0, 120, 155,  0.6); padding-top: 1px;">
                   Deudores
                   <div class="text-right">
                  <br/>
                    
                   </div>
                </div>

                <div class="card-body">
                   <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Cuenta</th>  
                            <th scope="col">Adeudo</th> 
                            <th colspan="3">Acciones</th>
                        </tr>

                    </thead>
                    <tbody>
                        @foreach($adeudos as $item)
                        <tr class="table-danger">
                       
                            <td>{{$item->usuario}}</td>
                             <td>{{$item->cuenta}}</td>
                              <td>{{$item->importe}}</td>

                            <td width="10px">
                       
                                <a href="{{route('adeudos.show',$item->id)}}" class="btn btn-sm btn-default">
                                  <i class="far fa-eye"></i>
                                </a>
             
                            </td>

                            <td width="10px">
                            
                                <a href="{{route('adeudos.edit',$item->id)}}" class="btn btn-sm btn-default" >
                                  Abonar
                                </a>
                             
                           
                            </td>


                        </tr>
                        @endforeach

                    </tbody>
                       

                   </table>
                   <br/>
                   {{--Paginación--}}
                    <div class="row justify-content-center responsive">                       
                       {{$adeudos->onEachSide(2)->links()}}                  
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection