<?php

namespace App\Http\Controllers;

use App\Http\Requests\CanalerosStoreRequest;
use App\Http\Requests\CanalerosUpdateRequest;
use Illuminate\Http\Request;
use App\Canaleros;
use DB;


class CanalerosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $canaleros = Canaleros::all();
        return view('canaleros.index',compact('canaleros'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CanalerosStoreRequest $request)
    {
        //
      
        $datosCanalero=request()->except('_token');
        Canaleros::insert($datosCanalero);

        return redirect('/canaleros')->with('Mensaje','¡Registro guardado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Canaleros  $canaleros
     * @return \Illuminate\Http\Response
     */
    public function show(Canaleros $canaleros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Canaleros  $canaleros
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $canaleros = DB::table('canalero')
                    ->where('id', '=', $id)
                    ->get();
        return view('canaleros.editar',compact('canaleros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Canaleros  $canaleros
     * @return \Illuminate\Http\Response
     */
    public function update(CanalerosUpdateRequest $request, $id)
    {
        //
      
        $datosCanalero=request()->except(['_token','_method']);
         Canaleros::where('id','=',$id)->update($datosCanalero);

        return redirect('/canaleros')->with('Mensaje','¡Registro modificado exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Canaleros  $canaleros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Canaleros::destroy($id);    
        return redirect('/canaleros')->with('Mensaje','¡Registro eliminado exitosamente!');
    }

    public function agregar(){
        return view('canaleros.nuevo');
    }

   

}
