 
@extends('admin.layout')

@section('content')



 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-info" role="alert">
       {{Session::get('Mensaje')}}
    </div>
@endif
<!-- Fin -->

<br/>

<form method="POST" action="{{ route('libro.store') }}">
  @csrf

<div class="container bg-white shadow p-2" >

    <div class="card-header text-white" style="background-color:rgba(0, 120, 155,  0.6);"><h4><strong>{{ __('Captura de libro') }}</strong></h4></div>
        
   <br/>

     <div class="row justify-content-center ">
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Nombre</span>
               <input id="usuario" type="text" class="form-control @error('usuario') is-invalid @enderror" name="usuario" value="{{ old('usuario') }}" required autocomplete="nombre" autofocus>
		                               @error('usuario')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >Tarjeta</span>
                <input id="tarjeta" type="text" class="form-control @error('tarjeta') is-invalid @enderror" name="tarjeta" value="0" required autocomplete="tarjeta" autofocus>
		                               @error('tarjeta')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3">
               <span class="help-block text-muted small-font" >CPL</span>
                <input id="cpl" type="text" class="form-control @error('cpl') is-invalid @enderror" name="cpl" value="0" required autocomplete="cpl" autofocus>
		                               @error('cpl')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
          </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >TSL</span>
                 <input id="tsl" type="text" class="form-control @error('tsl') is-invalid @enderror" name="tsl" value="0" required autocomplete="tsl" autofocus>
		                               @error('nombre')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >RA</span>
                <input id="ra" type="text" class="form-control @error('ra') is-invalid @enderror" name="ra" value="0" required autocomplete="ra" autofocus>
		                               @error('ra')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >PCO</span>
                 <input id="pco" type="text" class="form-control @error('pco') is-invalid @enderror" name="pco" value="0" required autocomplete="pco" autofocus>
		                               @error('pco')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >SR</span>
                 <input id="sr" type="text" class="form-control @error('sr') is-invalid @enderror" name="sr" value="0" required autocomplete="sr" autofocus>
		                               @error('sr')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >SUP_FIS</span>
                 <input id="sup_fis" type="text" class="form-control @error('sup_fis') is-invalid @enderror" name="sup_fis" value="0" required autocomplete="sup_fis" autofocus>
		                               @error('sup_fis')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P_DREN</span>
                 <input id="p_dren" type="text" class="form-control @error('p_dren') is-invalid @enderror" name="p_dren" value="0" required autocomplete="p_dren" autofocus>
		                               @error('p_dren')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
      </div>

      <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >S_DREN</span>
                 <input id="s_dren" type="text" class="form-control @error('s_dren') is-invalid @enderror" name="s_dren" value="0" required autocomplete="s_dren" autofocus>
		                               @error('s_dren')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >CULTIVO</span>
                  <select class="form-control" name="cultivo">
                    <option selected>--- Selecciona una opción ---</option>
                     @foreach($cultivos as $item)
                       <option>{{$item->cultivo}}</option>
                     @endforeach
                  </select>
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >HF</span>
                 <input id="hf" type="text" class="form-control @error('hf') is-invalid @enderror" name="hf" value="0" required autocomplete="hf" autofocus>
		                               @error('hf')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
      </div>

       <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P1</span>
                 <input id="p1" type="text" class="form-control @error('nombre') is-invalid @enderror" name="p1" value="0" required autocomplete="p1" autofocus>
		                               @error('p1')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >R1</span>
                 <input id="r1" type="text" class="form-control @error('r1') is-invalid @enderror" name="r1" value="0" required autocomplete="r1" autofocus>
		                               @error('r1')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >T1</span>
                 <input id="t1" type="text" class="form-control @error('t1') is-invalid @enderror" name="t1" value="0" required autocomplete="t1" autofocus>
		                               @error('t1')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
      </div>

       <div class="row justify-content-center">
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >P2</span>
                 <input id="p2" type="text" class="form-control @error('nombre') is-invalid @enderror" name="p2" value="0" required autocomplete="p2" autofocus>
		                               @error('p2')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
           <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >R2</span>
                 <input id="r2" type="text" class="form-control @error('r2') is-invalid @enderror" name="r2" value="0" required autocomplete="r2" autofocus>
		                               @error('r2')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <span class="help-block text-muted small-font" >Fecha</span>
                <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha" autofocus>
		                               @error('fecha')
		                                   <span class="invalid-feedback" role="alert">
		                                        	<strong>{{ $message }}</strong>
		                                    </span>
		                                @enderror
           </div>
      </div>

     

      <br/>

       <div class="row">
                            <div class="col text-center">
                              
                                  <a href="{{ route('libro.index') }}" class="btn btn-link">
                                   
                                     <h1> <i class="fas fa-times"></i></h1> 
                                    
                                 </a>
                                
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-link">
                                    <h1> <i class="far fa-save"></i></h1>
                                </button>
                            </div>
</form>

<!-- Adeudo -->
<form method="POST" action="{{ route('libro.adeudos') }}">
  @csrf



                           <!-- Button trigger modal -->
							<button type="button" class="btn btn-outline-secondary btn-lg" data-toggle="modal" data-target="#exampleModalCenter" style="margin-right: 10px">
							  Adeudo
							</button>

							<!-- Modal -->
							<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLongTitle">Adeudo</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">

								     <div class="row justify-content-center ">
									          <div class="col-md-3 col-sm-3 col-xs-3">
									               <span class="help-block text-muted small-font" >Nombre</span>
									               <input id="usuario" type="text" class="form-control @error('usuario') is-invalid @enderror" name="usuario" value="{{ old('usuario') }}" required autocomplete="usuario" autofocus>
											                               @error('usuario')
											                                   <span class="invalid-feedback" role="alert">
											                                        	<strong>{{ $message }}</strong>
											                                    </span>
											                                @enderror
									          </div>

									          <div class="col-md-3 col-sm-3 col-xs-3">
									               <span class="help-block text-muted small-font" >Tarjeta</span>
									               <input id="tarjeta" type="text" class="form-control @error('tarjeta') is-invalid @enderror" name="tarjeta" value="0" required autocomplete="tarjeta" autofocus>
											                               @error('tarjeta')
											                                   <span class="invalid-feedback" role="alert">
											                                        	<strong>{{ $message }}</strong>
											                                    </span>
											                                @enderror
									          </div>

									           <div class="col-md-3 col-sm-3 col-xs-3">
									               <span class="help-block text-muted small-font" >Cuenta</span>
									               <input id="cuenta" type="text" class="form-control @error('cuenta') is-invalid @enderror" name="cuenta" value="0" required autocomplete="cuenta" autofocus>
											                               @error('cuenta')
											                                   <span class="invalid-feedback" role="alert">
											                                        	<strong>{{ $message }}</strong>
											                                    </span>
											                                @enderror
									          </div>

     								 </div>

							  		<div class="row justify-content-center">
							           <div class="col-md-4 col-sm-4 col-xs-4">
							                <span class="help-block text-muted small-font" >Ejido</span>
							                 <input id="ejido" type="text" class="form-control @error('ejido') is-invalid @enderror" name="ejido" value="{{ old('usuario') }}" required autocomplete="sr" autofocus>
									                               @error('ejido')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>

							           

							            <div class="col-md-5 col-sm-5 col-xs-5">
							                <span class="help-block text-muted small-font" >Municipio</span>
							                  <select class="form-control" name="municipio">
			                                    <option selected>--- Selecciona una opción ---</option>
			                                    <option >Alvaro Obregon</option>
			                                    <option >Tarimbaro</option>
			                                  </select> 
									                               @error('municipio')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>

      								 </div>

      								  <div class="row justify-content-center">

      								  	<div class="col-md-2 col-sm-2 col-xs-2">
							                <span class="help-block text-muted small-font" >Riegos</span>
							                 <input id="riegos" type="text" class="form-control @error('riegos') is-invalid @enderror" name="riegos" value="0" required autocomplete="riegos" autofocus>
									                               @error('concepto')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>

      								  	 <div class="col-md-5 col-sm-5 col-xs-5">
							                <span class="help-block text-muted small-font" >Concepto</span>
										      <select class="form-control" name="concepto">
							                    <option selected>--- Selecciona una opción ---</option>
							                     @foreach($conceptos as $item)
							                       <option>{{$item->clave}}</option>
							                     @endforeach
							                  </select>
							           </div>


							            <div class="col-md-2 col-sm-2 col-xs-2">
							                <span class="help-block text-muted small-font" >Sup_física</span>
							                 <input id="sup_fis" type="text" class="form-control @error('sup_fis') is-invalid @enderror" name="superficie" value="0" required autocomplete="superficie" autofocus>
									                               @error('sup_fis')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>



      								  </div>








      								 <div class="row justify-content-center">
							           <div class="col-md-5 col-sm-5 col-xs-5">
							                <span class="help-block text-muted small-font" >Fecha</span>
							                 <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha" autofocus>
									                               @error('fecha')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>
							           <div class="col-md-4 col-sm-4 col-xs-4">
							                <span class="help-block text-muted small-font" >Importe</span>
							                 <input id="importe" type="text" class="form-control @error('importe') is-invalid @enderror bg-secondary" name="importe" value="0" required autocomplete="importe" autofocus>
									                               @error('importe')
									                                   <span class="invalid-feedback" role="alert">
									                                        	<strong>{{ $message }}</strong>
									                                    </span>
									                                @enderror
							           </div>
							         </div>

							        
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-link" data-dismiss="modal">
							        	 <h1> <i class="fas fa-times"></i></h1> 
							        </button>
							        <button type="submit" class="btn btn-link">
							        	 <h1> <i class="far fa-save"></i></h1>
							        </button>
							      </div>
							    </div>
							  </div>
							</div>
							   
</form>

                            </div>              
                        </div>
</div>
<br/>


@endsection