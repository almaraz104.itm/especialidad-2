<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos', function (Blueprint $table) {
            $table->id();

            $table->string('folio');
            $table->date('fecha');
            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->float('num_lote');
            $table->float('sup_fisica');
            $table->float('sup_siembra');
            $table->bigInteger('id_cultivo')->unsigned();
            $table->foreign('id_cultivo')->references('id')->on('cultivo');
            $table->float('vol_aut');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permisos');
    }
}
