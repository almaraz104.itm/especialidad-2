<?php

namespace App\Http\Controllers;

use App\Adeudos;
use DB;
use Illuminate\Http\Request;

class AdeudosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adeudos=Adeudos::paginate(20);
        return view ('adeudos.index',compact('adeudos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adeudos  $adeudos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $Adeudo = DB::table('adeudos')
                    ->where('id', '=', $id)
                    ->get();
        return view('adeudos.ver',compact('Adeudo'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adeudos  $adeudos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $Adeudo = DB::table('adeudos')
                    ->where('id', '=', $id)
                    ->get();
        return view('adeudos.editar',compact('Adeudo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adeudos  $adeudos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Recibo los datos del formulario 
         $datosAdeudo=request()->except(['_token','_method']);

         //Realizo la resta del importe y del abono 
         $adeudo = $request->get('importe');
         $abono = $request->get('abono');
         $importe = ($adeudo -  $abono);

        if ($importe == '0') {
            DB::table('ingresos')
                        ->where('id', '=', $id)
                        ->insert([
                            'factura' =>'NA',
                            'fecha' => $datosAdeudo['fecha'],
                            'tarjeta' => $datosAdeudo['tarjeta'],
                            'nombre' => $datosAdeudo['usuario'],
                            'ciclo' => $datosAdeudo['ciclo'],
                            'cuenta' => $datosAdeudo['cuenta'],
                            'seccion' => $datosAdeudo['seccion'],
                            'ejido' => $datosAdeudo['ejido'],
                            'municipio' => $datosAdeudo['municipio'],
                            'cultivo' => 'NA',
                            'superficie' => $datosAdeudo['superficie'],
                            'concepto' => $datosAdeudo['concepto'],
                            'tipo' => 'NA',
                            'unidad' => $datosAdeudo['unidad'],
                            'cuota2' => $datosAdeudo['cuota2'],
                            'riegos' => $datosAdeudo['riegos'],
                            'cantidad' => $datosAdeudo['cantidad'],
                            'total' => $datosAdeudo['importe'],
                            ]);
                //Borro el registro de adeudos y redirecciono con el nombre del cliente 
                    Adeudos::destroy($id); 
                    $pagada = $request->get('usuario');

                return redirect('/adeudos')->with('Mensaje','Adeudo pagado del cliente:  '.$pagada);


            }else{
                //Actualizo solo el importe 
              DB::table('adeudos')
                            ->where('id', '=', $id)
                            ->update([
                     'importe' => $importe,
                ]);


               return redirect('/adeudos')->with('Mensaje','¡Abono registrado exitosamente!');

            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adeudos  $adeudos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adeudos $adeudos)
    {
        //
    }
}
