<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CultivoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'clave' => ['required','unique:cultivo'],
            'cultivo' => ['required','string','min:1'],
            'fsiembra' => ['required','string','min:1'],
            'fcosecha' => ['required','string','min:1'],
            'fsoca' => ['required','string'],
            'fuente_ab' => ['required','string'],
        ];
    }
}
